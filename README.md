# README #
**Author :Rahul**

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* CCD2QOMOP jar parses a CCDA-Complaint xml document into one big clinical data object consisting of 10 different domain(Observation,Medications,Condition etc) collection objects.
* Version 1.0


### How do I get set up? ###

* QOMOPInfoBuilder is the entry point, pass in the CCD instance.
* Build Process : Run the job under following link
http://ec2-107-20-94-142.compute-1.amazonaws.com:8080/job/CCD2QOMOP/ 
* Artifactory: The jar ends up in the following maven repository.
  (http://ec2-107-20-94-142.compute-1.amazonaws.com:8081/artifactory/)
* **Credentials for Artifactory login: admin,password**