package com.quintiles.qomop.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.qomop.builders.PersonInfoBuilder;
import com.quintiles.qomop.main.QOMOP;

public class PersonInfoBuilderTest extends BuilderTest {

	static ContinuityOfCareDocument ccd;
	static QOMOP omop = new QOMOP();

	@BeforeClass
	public static void setUp() {
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
		PersonInfoBuilder.build(ccd, omop);
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Create_Date() {
		assertThat(omop.getPerson().getCreateDate(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Location_Sid() {
		assertThat(omop.getPerson().getLocationSid(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_ethinicity_Cd_Null() {
		assertThat(omop.getPerson().getEthinicityCdNull(), is("NI"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Ethinicity_Source_Value() {
		assertThat(omop.getPerson().getEthinicitySourceValue(), is("Not Hispanic or Latino"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Ethinicity_Cd_Sys_Name() {
		assertThat(omop.getPerson().getEthinicityCdSysName(), is("TEST"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Ethinicity_Cd_Sys() {
		assertThat(omop.getPerson().getEthinicityCdSys(), is("2.16.840.1.113883.6.238"));
	}

	

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Ethinicity_Cd_Value() {
		assertThat(omop.getPerson().getEthinicityCdValue(), is("2186-5"));
	}
	
	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Source_Member_Id() {
		assertThat(omop.getPerson().getSourceMemberId(), is("42"));
	}
	

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_ethnicity_concept_id() {
		assertThat(omop.getPerson().getEthnicityConceptId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Race_Cd_Null() {
		assertThat(omop.getPerson().getRaceCdNull(), is("NI"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Race_Source_Value() {
		assertThat(omop.getPerson().getRaceSourceValue(), is("White"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Race_Cd_Sys_Name() {
		assertThat(omop.getPerson().getRaceCdSysName(), is("TEST"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_race_cd_sys() {
		assertThat(omop.getPerson().getRaceCdSys(), is("2.16.840.1.113883.6.238"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_race_cd_value() {
		assertThat(omop.getPerson().getRaceCdValue(), is("2106-3"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_race_concept_id() {
		assertThat(omop.getPerson().getRaceConceptId(), is(notNullValue()));
	}
	
	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Provider_Id() {
		assertThat(omop.getPerson().getProviderId(), is("1681891304256110"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_Location_Id() {
		assertThat(omop.getPerson().getLocationId(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Day_Of_Birth() {
		assertThat(omop.getPerson().getDayOfBirth(), is("22"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Month_Of_Birth() {
		// Calender month logic adds +1 to value as the months range from 0-11 in CCD
		assertThat(omop.getPerson().getMonthOfBirth(), is("1"));	
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Year_Of_Birth() {
		assertThat(omop.getPerson().getYearOfBirth(), is("1936"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Gender_Cd_Null() {
		assertThat(omop.getPerson().getGenderCdNull(), is("UNK"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Gender_Source_Value() {
		assertThat(omop.getPerson().getGenderSourceValue(), is("Male"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_gender_cd_sys_name() {
		assertThat(omop.getPerson().getGenderCdSysName(), is("HL7AdministrativeGender"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_gender_cd_sys() {
		assertThat(omop.getPerson().getGenderCdSys(), is("2.16.840.1.113883.5.1"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_gender_cd_value() {
		assertThat(omop.getPerson().getGenderCdValue(), is("M"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_Gender_Concept_Id() {
		assertThat(omop.getPerson().getGenderConceptId(), is(notNullValue()));
	}

	@Test
	@Category(IgnoredTestCategory.class)
	public void testPerson_person_source_value() {
		assertThat(omop.getPerson().getPersonSourceValue(), is("42:1346775430700600"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_Source_Key() {
		assertThat(omop.getPerson().getSourceKey(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testPerson_Person_Id() {
		assertThat(omop.getPerson().getPersonId(), is(notNullValue()));
	}

	@Ignore
	@Category(GoodTestCategory.class)
	public void testPerson_validationFailure() {
		
	}
	

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testPerson_Care_Site_Id() {
		assertThat(omop.getPerson().getCareSiteId(), is(""));
	}

}
