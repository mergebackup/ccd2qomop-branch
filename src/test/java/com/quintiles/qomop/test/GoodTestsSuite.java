package com.quintiles.qomop.test;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Categories.class)
@IncludeCategory(GoodTestCategory.class)
@ExcludeCategory(IgnoredTestCategory.class)
@SuiteClasses({ ConditionOccurrenceInfoAllergyBuilderTest.class,
		ConditionOccurrenceInfoProblemBuilderTest.class,
		OrganizationInfoBuilderTest.class, PersonInfoBuilderTest.class,
		ProcedureOccurrenceInfoBuilderTest.class,
		ProviderInfoBuilderTest.class, VisitOccurrenceInfoBuilderTest.class })
public class GoodTestsSuite {

}
