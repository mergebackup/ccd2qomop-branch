package com.quintiles.qomop.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.ConditionOccurrence;
import com.quintiles.qomop.builders.ConditionOccurrenceInfoBuilder;

public class ConditionOccurrenceInfoAllergyBuilderTest extends BuilderTest {

	static ContinuityOfCareDocument ccd;
	static List<ConditionOccurrence> conditionOccList = new ArrayList<ConditionOccurrence>();

	@BeforeClass
	public static void setUp() {
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
		ConditionOccurrenceInfoBuilder.handleAllergySection(ccd,
				conditionOccList);
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Source_Key() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(
				condition.getSourceKey(),
				is("1.2.840.113619.21.1.8175993882063780852.3.1:2779-1280947838000:20100804135157"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Source_Member_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(
				condition.getSourceMemberId(),
				is("1000-4-9390"));
	}
	
	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Occurrence_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);

		assertThat(condition.getConditionOccurrenceId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Created_Date() {
		ConditionOccurrence condition = conditionOccList.get(0);

		assertThat(condition.getCreateDate(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Concept_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);

		assertThat(condition.getConditionConceptId(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Source_Value() {
		ConditionOccurrence condition = conditionOccList.get(0);

		assertThat(condition.getConditionSourceValue(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Person_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(
				condition.getPersonId(),
				is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Sid() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionSid(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Associated_Provider_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getAssociatedProviderId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Cd_Display() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionCdDisplay(),
				is("Natural Latex Rubber"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Cd_Null() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionCdNull(), is("NI"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Cd_Sys() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionCdSys(), is("2.16.840.1.113883.4.9"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Cd_Sys_Name() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionCdSysName(), is("UNII"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Cd_Sys_Value() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionCdValue(), is("2LQ0UUW8IN"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Cd_Display() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeCdDisplay(),
				is("GE No Known Allergies"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Cd_Null() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeCdNull(), is("UNK"));

	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Cd_Sys() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeCdSys(),
				is("2.16.840.1.113883.6.96"));

	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Cd_Sys_Name() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeCdName(), is("SNOMEDCT"));

	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Cd_Sys_Value() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeCdValue(), is("419199007"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Type_Concept_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionTypeConceptId(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Stop_Reason() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getStopReason(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Associate_Provider_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getAssociatedProviderId(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Visit_Occurrence_Id() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getVisitOccurrenceId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_Start_Date() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionStartDate(),
				is("2010-08-04 18:50:38.000"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testAllergiesSection_ConditionOccurrence_Condition_End_Date() {
		ConditionOccurrence condition = conditionOccList.get(0);
		assertThat(condition.getConditionEndDate(),
				is("2010-08-04 18:50:38.000"));
	}	
}
