package com.quintiles.qomop.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.VisitOccurrence;
import com.quintiles.qomop.builders.VisitOccurrenceInfoBuilder;
import com.quintiles.qomop.main.QOMOP;

public class VisitOccurrenceInfoBuilderTest extends BuilderTest {

	static ContinuityOfCareDocument ccd;
	
	static List<VisitOccurrence> visitOccurrences = new ArrayList<VisitOccurrence>();

	static QOMOP qomop = new QOMOP();

	@BeforeClass
	public static void setUp() {
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
		VisitOccurrenceInfoBuilder.handleVisitOccurrence(ccd, qomop);
		visitOccurrences = qomop.getVisitOccList();
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Create_Date() {		
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getCreateDate(), is(notNullValue()));
	}

	@Test
	@Category(IgnoredTestCategory.class)
	public void testVisit_Source_Member_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getSourceMemberId(), is("42"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Person_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getPersonId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Source_Key_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getSourceKey(), is("1.2.840.113619.21.1.8175993882063780852.3.10:1596550708206210"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testVisit_Visit_Sid() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitSid(), is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testVisit_Visit_Occurrence_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitOccurrenceId(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_Start_Date() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitStartDate(), is("2010-08-04 07:00:00.000"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_End_Date() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitEndDate(), is("2010-08-04 21:19:28.000"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testVisit_Place_Of_Service_Concept_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getPlaceOfServiceConceptId(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_Cd_Value() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitCdValue(), is("99215"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_Cd_Sys() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitCdSys(), is("2.16.840.1.113883.6.12"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_Cd_Sys_Name() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitCdSysName(), is("C4"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Visit_Cd_Null() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getVisitCdNull(), is("NA"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Place_Of_Service_Source_Value() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getPlaceOfServiceSourceValue(), is("Ofc Vst, Est Level V"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testVisit_Care_Site_Id() {
		VisitOccurrence visit = visitOccurrences.get(0);
		assertThat(visit.getCareSiteId(), is(notNullValue()));
	}

}
