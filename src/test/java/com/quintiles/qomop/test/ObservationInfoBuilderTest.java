package com.quintiles.qomop.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.Observation;
import com.quintiles.qomop.builders.ObservationInfoBuilder;

public class ObservationInfoBuilderTest extends BuilderTest
{
	private static ContinuityOfCareDocument ccd;

	@BeforeClass
	public static void setUp()
	{
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
	}

	@Test
	public void testResultsSection()
	{
		List<Observation> observationList = new ArrayList<Observation>();
		//ObservationInfoBuilder.build(ccd, new QOMOP());
		ObservationInfoBuilder.handleResultSection(observationList, ccd);
		Observation ob = observationList.get(0);
		Assert.assertEquals("ObservationSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.6:79408-1275678660000:20100607160236",ob.getSourceKey());
		Assert.assertEquals("ObservationCdNull mismatch","UNK",ob.getObservationCdNull());
		Assert.assertEquals("ObservationCdValue mismatch","UNK_CD",ob.getObservationCdValue());
		Assert.assertEquals("ObservationCdSys mismatch","2.16.840.1.113883.6.1",ob.getObservationCdSys());
		Assert.assertEquals("ObservationCdSystemName mismatch","LOINC",ob.getObservationCdSysName());
		Assert.assertEquals("ObservationSourceValue mismatch","Vitamin D, 25 Hydroxy D2",ob.getObservationSourceValue());
		Assert.assertEquals("ObservationStartDate mismatch", "2010-06-04 19:11:00.000", ob.getObservationStartDate());
		Assert.assertEquals("ObservationEndDate mismatch", "2010-06-04 19:11:00.000", ob.getObservationEndDate());
		Assert.assertEquals("ObservationValueAsNumber mismatch", "35.0", ob.getValueAsNumber());
		Assert.assertEquals("ObservationUnitSourceValue mismatch", "ng/mL", ob.getUnitsSourceValue());
		
	}
	@Test
	public void testVitalSection()
	{
		List<Observation> observationList = new ArrayList<Observation>();
		ObservationInfoBuilder.handleVitalSection(observationList, ccd);
		Observation ob = observationList.get(0);
		Assert.assertEquals("ObservationSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.7:53-1294256047000:20110105113439",ob.getSourceKey());
		Assert.assertEquals("ObservationCdNull mismatch","ASKU",ob.getObservationCdNull());
		Assert.assertEquals("ObservationCdValue mismatch","8462-4",ob.getObservationCdValue());
		Assert.assertEquals("ObservationCdSys mismatch","2.16.840.1.113883.6.1",ob.getObservationCdSys());
		Assert.assertEquals("ObservationCdSystemName mismatch","LOINC",ob.getObservationCdSysName());
		Assert.assertEquals("ObservationSourceValue mismatch","BP Diastolic",ob.getObservationSourceValue());
		Assert.assertEquals("ObservationStartDate mismatch", "2011-01-05 19:34:07.000", ob.getObservationStartDate());
		Assert.assertEquals("ObservationEndDate mismatch", "2011-01-05 19:34:07.000", ob.getObservationEndDate());
		Assert.assertEquals("ObservationValueAsNumber mismatch", "84.0", ob.getValueAsNumber());
		Assert.assertEquals("ObservationUnitSourceValue mismatch", "mm[Hg]", ob.getUnitsSourceValue());
		
	}
	
	
	
}
