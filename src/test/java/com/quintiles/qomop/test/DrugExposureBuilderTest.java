package com.quintiles.qomop.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.DrugExposure;
import com.quintiles.qomop.builders.DrugExposureInfoBuilder;

public class DrugExposureBuilderTest extends BuilderTest
{

	private static ContinuityOfCareDocument ccd;

	@BeforeClass
	public static void setUp()
	{
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
	}

	@Test
	public void testMedicationSection()
	{
		List<DrugExposure> drugExposureList = new ArrayList<DrugExposure>();
		DrugExposureInfoBuilder.handleMedicationSection(ccd, drugExposureList);
		DrugExposure drug = drugExposureList.get(0);
		Assert.assertEquals("DrugSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.2:1682003473006120:20130419151112",drug.getSourceKey());
		//Assert.assertEquals("ObservationSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.2:1682003473006120:20130419151112",drug.getSourceKey());
		Assert.assertEquals("DrugCdNull mismatch","ASKU",drug.getDrugCdNull());
		Assert.assertEquals("DrugCdValue mismatch","630208",drug.getDrugCdValue());
		Assert.assertEquals("DrugCdSys mismatch","2.16.840.1.113883.6.88",drug.getDrugCdSys());
		Assert.assertEquals("DrugCdSystemName mismatch","RxNorm",drug.getDrugCdSysName());
		Assert.assertEquals("DrugSourceValue mismatch","ALBUTEROL SULFATE",drug.getDrugSourceValue());
		Assert.assertEquals("DrugStartDate mismatch", "2013-04-19 07:00:00.000", drug.getDrugExposureStartDate());
		Assert.assertEquals("DrugStartDate mismatch", null, drug.getDrugExposureEndDate());
	}
	
	
	@Test
	public void testImmunizationSection()
	{
		List<DrugExposure> drugExposureList = new ArrayList<DrugExposure>();
		DrugExposureInfoBuilder.handleImmunizationSection(ccd, drugExposureList);
		DrugExposure drug = drugExposureList.get(0);
		Assert.assertEquals("DrugSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.2:1682003473006120:20130419151112",drug.getSourceKey());
		//Assert.assertEquals("ObservationSourceKey mismatch","1.2.840.113619.21.1.8175993882063780852.3.2:1682003473006120:20130419151112",drug.getSourceKey());
		Assert.assertEquals("DrugCdNull mismatch","ASKU",drug.getDrugCdNull());
		Assert.assertEquals("DrugCdValue mismatch","630208",drug.getDrugCdValue());
		Assert.assertEquals("DrugCdSys mismatch","2.16.840.1.113883.6.88",drug.getDrugCdSys());
		Assert.assertEquals("DrugCdSystemName mismatch","RxNorm",drug.getDrugCdSysName());
		Assert.assertEquals("DrugSourceValue mismatch","ALBUTEROL SULFATE",drug.getDrugSourceValue());
		Assert.assertEquals("DrugStartDate mismatch", "2013-04-19 07:00:00.000", drug.getDrugExposureStartDate());
		Assert.assertEquals("DrugStartDate mismatch", null, drug.getDrugExposureEndDate());

		

	}
	
}
