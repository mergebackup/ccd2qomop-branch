package com.quintiles.qomop.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.Organization;
import com.quintiles.qomop.builders.OrganizationInfoBuilder;
import com.quintiles.qomop.main.QOMOP;

public class OrganizationInfoBuilderTest extends BuilderTest {
	static ContinuityOfCareDocument ccd;
	static QOMOP omop = new QOMOP();
	static List<Organization> organizationList;

	@BeforeClass
	public static void setUp() {
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
		OrganizationInfoBuilder.build(ccd, omop);
		organizationList = omop.getOrganizationList();
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Create_Date() {
		assertThat(omop.getOrganizationList().get(0).getCreateDate(),
				is(notNullValue()));
	}

	@Test
	@Category(IgnoredTestCategory.class)
	public void testOrganization_Source_Member_Id() {
		assertThat(omop.getOrganizationList().get(0).getSourceMemberId(),
				is("1000-4-9390"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Person_Id() {
		assertThat(omop.getOrganizationList().get(0).getPersonId(),
				is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Source_Key_Id() {
		assertThat(omop.getOrganizationList().get(0).getSourceKey(),
				is("1000-4-9390:0"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testOrganization_Organization_Sid() {
		assertThat(omop.getOrganizationList().get(0).getOrganizationSid(),
				is(notNullValue()));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testOrganization_Organization_Id() {
		assertThat(omop.getOrganizationList().get(0).getOrganizationSid(),
				is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Member_Id() {
		assertThat(omop.getOrganizationList().get(0).getMemberId(), is("1000-4-9390"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testOrganization_Location_Id() {
		assertThat(omop.getOrganizationList().get(0).getLocationId(), is(""));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testOrganization_Place_Of_Service_Concept_Id() {
		assertThat(omop.getOrganizationList().get(0)
				.getPlaceOfServiceConceptId(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Organization_Cd_Value() {

		assertThat(omop.getOrganizationList().get(0).getPosCdValue(),
				is("UNK_CD"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Organization_Cd_Sys() {

		assertThat(omop.getOrganizationList().get(0).getPosCdSys(),
				is("UNK_VOC"));
	}

	@Ignore
	@Category(GoodTestCategory.class)
	public void testOrganization_Organization_Cd_Sys_Name() {

		assertThat(omop.getOrganizationList().get(0).getPosCdSysName(),
				is(""));
	}

	@Ignore
	@Category(GoodTestCategory.class)
	public void testOrganization_Organization_Cd_Null() {

		assertThat(omop.getOrganizationList().get(0).getPosCdNull(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Organization_Service_Source_Value() {

		assertThat(omop.getOrganizationList().get(0)
				.getOrganizationSourceValue(), is("Jeffrey Feldman M.D."));
	}
	
	@Ignore
	@Category(GoodTestCategory.class)
	public void testOrganization_Place_Of_Service_Source_Value() {

		assertThat(omop.getOrganizationList().get(0)
				.getPlaceOfServiceSourceValue(), is(""));
	}

	
	
	@Test
	@Category(GoodTestCategory.class)
	public void testOrganization_Location_Sid() {

		assertThat(omop.getOrganizationList().get(0).getLocationSid(),
				is(notNullValue()));
	}

}
