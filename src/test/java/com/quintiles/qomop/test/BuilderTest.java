package com.quintiles.qomop.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.openhealthtools.mdht.uml.cda.consol.ConsolPackage;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.util.CDAUtil;
import org.openhealthtools.mdht.uml.cda.util.ValidationResult;

public class BuilderTest {

	public BuilderTest() {
		super();
	}

	public static ContinuityOfCareDocument loadCCD(String XML) {
			// *** Must be run to initialize MDHT
			ConsolPackage.eINSTANCE.eClass();
			ClassLoader classLoader = BuilderTest.class.getClassLoader();
	        File file = new File(classLoader.getResource(XML).getFile());
			ContinuityOfCareDocument ccd = null;
			FileInputStream fis;
			try
			{
				fis = new FileInputStream(file);
				ccd = getDocumentFromStream(fis);
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			return ccd;
	
		}

    public static ContinuityOfCareDocument loadCCD(File f) {
        // *** Must be run to initialize MDHT
        ContinuityOfCareDocument ccd = null;
        FileInputStream fis;
        try {
            fis = new FileInputStream(f);
            ccd = getDocumentFromStream(fis);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return ccd;

    }

    public static ContinuityOfCareDocument getDocumentFromStream(
			final InputStream is)
	{
		try
		{
			return (ContinuityOfCareDocument) CDAUtil.load(is,
					new ValidationResult());
		}
		catch (Exception e)
		{
			System.err.println("Unable to parse the XML document");
		}
		return null;
	}

}