package com.quintiles.qomop.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.ProcedureOccurrence;
import com.quintiles.omop.VisitOccurrence;
import com.quintiles.qomop.builders.ProcedureOccurrenceInfoBuilder;
import com.quintiles.qomop.main.QOMOP;

public class ProcedureOccurrenceInfoBuilderTest extends BuilderTest {

	private static ContinuityOfCareDocument ccd;
	private static QOMOP qomop = new QOMOP();

	@BeforeClass
	public static void setUp() {
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
		ProcedureOccurrenceInfoBuilder.build(ccd, qomop);
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Create_Date() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getCreateDate(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Source_Member_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getSourceMemberId(), is("42"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Person_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getPersonId(), is(notNullValue()));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Source_Key_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getSourceKey(), is("1.2.840.113619.21.1.8175993882063780852.3.8:1596550741156210:20100804141900"));
	}

	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Sid() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureSid(), is(""));
	}

	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Occurrence_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureOccurrenceId(), is(""));
	}

	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Concept_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureConceptId(), is(""));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Cd_Value() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureCdValue(), is("TEST"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Cd_Sys() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureCdSys(), is("2.16.840.1.113883.6.96"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Cd_Sys_Name() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureCdSysName(), is("SNOMED CT"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Cd_Null() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureCdNull(), is("NA"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_CD_Sys_Name() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureCdSysName(), is("SNOMED CT"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Source_Value() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureSourceValue(), is("  ALL PRSC DUR ENCNTR QUAL E-PRSC SYS"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_Date() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureDate(), is("2010-08-04 07:00:00.000"));
	}

	@Test
	@Category(GoodTestCategory.class)
	public void testProcedure_Procedure_End_Date() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureEndDate(), is("2010-08-04 21:19:28.000"));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testProcedure_Visit_Occurrence_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getVisitOccurrenceId(), is(""));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testProcedure_Relevant_Condition_Concept_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getRelevantConditionConceptId(), is(""));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testProcedure_Care_Site_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getCaresiteId(), is(""));
	}

	@Ignore
	@Category(IgnoredTestCategory.class)
	public void testProcedure_Procedure_Type_Concept_Id() {
		ProcedureOccurrence procedure = qomop.getProcedureOcclist().get(0);
		assertThat(procedure.getProcedureTypeConceptId(), is(""));
	}

}
