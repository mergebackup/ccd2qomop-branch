package com.quintiles.qomop.test;

import java.io.File;
//import org.apache.avro.file.DataFileWriter;
//import org.apache.avro.generic.GenericDatumWriter;
//import org.apache.avro.io.DatumWriter;
//import org.apache.avro.specific.SpecificDatumWriter;
//import org.apache.avro.specific.SpecificRecordBase;
import org.junit.Ignore;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.qomop.builders.QOMOPInfoBuilder;
import com.quintiles.qomop.main.QOMOP;


public class QOMOPBuilderTest  extends BuilderTest
{

    public static final String AVRO_EXTENSION = ".avro";
//    public static final String XML = "9-1497027855458000.xml";
    public static final String XML = "71-1497027868346000.xml";
    public static String outputLocation = "C:/downloads/data/output/avro/";
    public static String inputDirectoryPath = "C:/downloads/data/input/avro/";

    /*
   * We can use this test class file trava 
   */
	@Ignore
	public void testBuild()
	{

		ContinuityOfCareDocument ccd = loadCCD(XML);
        QOMOP qomopData = QOMOPInfoBuilder.build(ccd);
 //       persistQOMOPData(qomopData);
	}

    public void testDirectoryBuild()  {
        File dir = new File(inputDirectoryPath);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                // Do something with child
                ContinuityOfCareDocument ccd = loadCCD(XML);
                QOMOP qomopData = QOMOPInfoBuilder.build(ccd);
//                persistQOMOPData(qomopData);
            }
        } else {
            // Handle the case where dir is not really a directory.
            // Checking dir.isDirectory() above would not be sufficient
            // to avoid race conditions with another process that deletes
            // directories.
            System.err.println("Hey this doesn't look like a directory");
        }

    }
/*

    public void persistQOMOPData(QOMOP qomop) {
        if (qomop.getPerson() != null)
            avroLocalSerialize(qomop.getPerson());
        if (qomop.getObservationList() != null) {
            for ( Observation observation : qomop.getObservationList() )
            {
            avroLocalSerialize(observation);
        }
	}
        if (qomop.getDrugExpList() != null) {
            for (DrugExposure drug : qomop.getDrugExpList()) {
                avroLocalSerialize(drug);
            }
        }
        if (qomop.getProcedureOcclist() != null) {
            for (ProcedureOccurrence procs : qomop.getProcedureOcclist()) {
                avroLocalSerialize(procs);
            }
        }
        if (qomop.getOrganizationList() != null) {
            for (Organization orgs : qomop.getOrganizationList()) {
                avroLocalSerialize(orgs);
            }
        }
        if (qomop.getConditionOccList() != null) {
            for (ConditionOccurrence conds : qomop.getConditionOccList()) {
                avroLocalSerialize(conds);
            }
        }
    }

	
	
	
	

	private void avroLocalSerialize(Object theObject) {

        Class theClass = getFileClass(theObject);
        try {
            String fileName = outputLocation + theClass.getSimpleName() + AVRO_EXTENSION;
            File theFile = new File(fileName);
            if (!theFile.exists()) {

                DatumWriter datumWriter = new SpecificDatumWriter(theClass);//@todo:will this work on HDFS
                DataFileWriter dataFileWriter = new DataFileWriter(datumWriter);
                SpecificRecordBase srb = SpecificRecordBase.class.cast(theObject);
                dataFileWriter.create(srb.getSchema(), theFile);
                dataFileWriter.append(theClass.cast(theObject));
                dataFileWriter.close();
            } else {

                DataFileWriter<Object> dataFileWriter = new DataFileWriter<Object>(new GenericDatumWriter<Object>()).appendTo(theFile);

//                dataFileWriter.appendTo(new FsInput(outputLocation),fos); use this if storing in HDFS
                dataFileWriter.append(theObject);
                dataFileWriter.close();

            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	*/

    protected Class getFileClass(Object classObject) {
       return classObject.getClass();
    }



    protected String[] checkListCamelCase(String[] oldList) {
        String[] newList = new String[oldList.length];
        for (int i = 0; i < oldList.length; i++) {
            String string = oldList[i];
            newList[i] = toCamelCase(string);
        }
        return newList;
    }

    public static String toCamelCase(String string) {
        StringBuffer result = new StringBuffer(string);
        while (result.indexOf("_") != -1) { //$NON-NLS-1$
            int indexOf = result.indexOf("_"); //$NON-NLS-1$
            result.replace(indexOf, indexOf + 2, "" + Character.toUpperCase(result.charAt(indexOf + 1))); //$NON-NLS-1$
        }
        return result.toString();
    }



    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("You must supply an output directory (Full path)");
            System.exit(1);
        } else if (!args[0].endsWith("/")) {
            outputLocation = args[0] + "/";
        }
    }
}
