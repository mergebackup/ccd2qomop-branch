package com.quintiles.qomop.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;

import com.quintiles.omop.Provider;
import com.quintiles.qomop.builders.ProviderInfoBuilder;

public class ProviderInfoBuilderTest extends BuilderTest
{
	private static ContinuityOfCareDocument ccd;

	@BeforeClass
	public static void setUp()
	{
		String theGoodCCD = "6-1346775430700600.xml";
		ccd = loadCCD(theGoodCCD);
	}

	@Test
	public void testProvider()
	{
		List<Provider> providerList = new ArrayList<Provider>();
		ProviderInfoBuilder.handleProvider(ccd, providerList);
		Provider provider = providerList.get(0);
		//Assert.assertEquals("ProviderId mismatch","1681891304256110",provider.getProviderId());
		Assert.assertEquals("ProviderCdNull mismatch","NI",provider.getSpecialtyCdNull());
		Assert.assertEquals("ProviderCdValue mismatch","UNK_CD",provider.getSpecialtyCdValue());
		Assert.assertEquals("ProviderCdSys mismatch","2.16.840.1.113883.6.101",provider.getSpecialtyCdSys());
		Assert.assertEquals("ProviderCdSystemName mismatch","NUCC",provider.getSpecialtyCdSysName());
		Assert.assertEquals("ProviderSourceValue mismatch","Jia Doc1",provider.getProviderSourceValue());

	}

}
