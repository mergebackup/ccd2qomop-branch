package com.quintiles.qomop.main;

import java.util.ArrayList;
import java.util.List;

import com.quintiles.omop.CareSite;
import com.quintiles.omop.ConditionOccurrence;
import com.quintiles.omop.DrugExposure;
import com.quintiles.omop.Location;
import com.quintiles.omop.Observation;
import com.quintiles.omop.Organization;
import com.quintiles.omop.Person;
import com.quintiles.omop.ProcedureOccurrence;
import com.quintiles.omop.Provider;
import com.quintiles.omop.VisitOccurrence;

public class QOMOP
{
	private Person person = new Person();
	private List<CareSite> careSiteList = new ArrayList<CareSite>();
	private List<ConditionOccurrence> conditionOccList = new ArrayList<ConditionOccurrence>();
	private List<DrugExposure> drugExpList = new ArrayList<DrugExposure>();
	private List<Location> locationList = new ArrayList<Location>();
	private List<Observation> observationList = new ArrayList<Observation>();
	private List<Organization> organizationList = new ArrayList<Organization>();
	private List<ProcedureOccurrence> procedureOcclist = new ArrayList<ProcedureOccurrence>();
	private List<Provider> providerList = new ArrayList<Provider>();
	private List<VisitOccurrence> visitOccList = new ArrayList<VisitOccurrence>();

	public Person getPerson()
	{
		return person;
	}

	public void setPerson(Person person)
	{
		this.person = person;
	}

	public void setCareSiteList(List<CareSite> careSiteList)
	{
		this.careSiteList = careSiteList;
	}

	public void setConditionOccList(List<ConditionOccurrence> conditionOccList)
	{
		this.conditionOccList = conditionOccList;
	}

	public void setDrugExpList(List<DrugExposure> drugExpList)
	{
		this.drugExpList = drugExpList;
	}

	public void setLocationList(List<Location> locationList)
	{
		this.locationList = locationList;
	}

	public void setObservationList(List<Observation> observationList)
	{
		this.observationList = observationList;
	}

	public void setOrganizationList(List<Organization> organizationList)
	{
		this.organizationList = organizationList;
	}

	public void setProcedureOcclist(List<ProcedureOccurrence> procedureOcclist)
	{
		this.procedureOcclist = procedureOcclist;
	}

	public void setProviderList(List<Provider> providerList)
	{
		this.providerList = providerList;
	}

	public void setVisitOccList(List<VisitOccurrence> visitOccList)
	{
		this.visitOccList = visitOccList;
	}

	public List<CareSite> getCareSiteList()
	{
		return careSiteList;
	}

	public List<ConditionOccurrence> getConditionOccList()
	{
		return conditionOccList;
	}

	public List<DrugExposure> getDrugExpList()
	{
		return drugExpList;
	}

	public List<Location> getLocationList()
	{
		return locationList;
	}

	public List<Observation> getObservationList()
	{
		return observationList;
	}

	public List<Organization> getOrganizationList()
	{
		return organizationList;
	}

	public List<ProcedureOccurrence> getProcedureOcclist()
	{
		return procedureOcclist;
	}

	public List<Provider> getProviderList()
	{
		return providerList;
	}

	public List<VisitOccurrence> getVisitOccList()
	{
		return visitOccList;
	}

}
