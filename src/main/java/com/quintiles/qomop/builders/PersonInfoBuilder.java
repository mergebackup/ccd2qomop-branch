package com.quintiles.qomop.builders;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.DocumentationOf;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Performer1;
import org.openhealthtools.mdht.uml.cda.ServiceEvent;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;

import com.quintiles.omop.Location;
import com.quintiles.omop.Person;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class PersonInfoBuilder extends Builder {

	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {
		// Avro Person class
		Person person = new Person();
		EList<PatientRole> patientRoles = ccd.getPatientRoles();
		if ((patientRoles != null) && (patientRoles.size() > 0)) {
			for (PatientRole patientRole : patientRoles) {
				

				EList<AD> addrs = patientRole.getAddrs();
				// Only one addrs under PatientRole
				if (addrs.size() > 0) {
					Location loc = OMOPUtils.getLocation(addrs.get(0), ccd);
					qomop.getLocationList().add(loc);
					person.setLocationSid(loc.getLocationSid());
				}

				patientProviderOrg = getOrganizationInfo(patientRole);
				person.setSourceKey(patientProviderOrg.getMemberId() + ":"
						+ OMOPUtils.extractPatientId(patientRole));
				
				person.setPersonId(OMOPUtils.getHashValue(person.getSourceKey()));

				person.setSourceMemberId(patientProviderOrg.getMemberId());
				person.setProviderId(getProviderId(ccd));
				person.setPersonSourceValue(patientProviderOrg.getMemberId()
						+ ":" + OMOPUtils.extractPatientId(patientRole));
				buildPatientInfo(patientRole, person);

			}

		}

		person.setCreateDate(ccd.getEffectiveTime().getValue());
		qomop.setPerson(person);

	}

	private static String getProviderId(ContinuityOfCareDocument ccd) {
		if (ccd.getDocumentationOfs() != null) {
			DocumentationOf document = ccd.getDocumentationOfs().get(0);
			ServiceEvent sEvent = document.getServiceEvent();
			if (sEvent != null && sEvent.getPerformers() != null
					&& sEvent.getPerformers().size() > 0) {
				Performer1 provider = sEvent.getPerformers().get(0);
				if (isValidEntry(provider.getNullFlavor())) {

					return ProviderInfoBuilder.extractProviderId(provider);
				}

			}
		}
		return null;
	}

	public static void buildPatientName(PatientRole patientRole, Person person) {
		StringBuffer name = new StringBuffer();
		if (patientRole.getPatient().getNames() != null
				&& patientRole.getPatient().getNames().size() > 0) {
			if (patientRole.getPatient().getNames().get(0).getGivens() != null) {

				if (patientRole.getPatient().getNames().get(0).getGivens()
						.size() > 0
						&& patientRole.getPatient().getNames().get(0)
								.getGivens().get(0) != null) {
					name.append(patientRole.getPatient().getNames().get(0)
							.getGivens().get(0).getText());
				}
				if (patientRole.getPatient().getNames().get(0).getGivens()
						.size() > 1
						&& patientRole.getPatient().getNames().get(0)
								.getGivens().get(1) != null) {
					name.append(" ").append(
							patientRole.getPatient().getNames().get(0)
									.getGivens().get(1).getText());
				}
				if (patientRole.getPatient().getNames().get(0).getFamilies()
						.size() > 0
						&& patientRole.getPatient().getNames().get(0)
								.getFamilies().get(0) != null) {
					name.append(" ").append(
							patientRole.getPatient().getNames().get(0)
									.getFamilies().get(0).getText());
				}
				person.setPersonSourceValue(name.toString());
			}
		}
	}

	public static void buildPatientInfo(PatientRole patientRole, Person person) {

		// buildPatientName(patientRole, person);
		if (patientRole.getPatient() != null) {
			if (patientRole.getPatient().getAdministrativeGenderCode() != null) {
				person.setGenderCdSysName(patientRole.getPatient()
						.getAdministrativeGenderCode().getCodeSystemName());
				person.setGenderCdSys(getVocabSys(patientRole.getPatient()
						.getAdministrativeGenderCode().getCodeSystem()));
				person.setGenderCdValue(getCodeValue(patientRole.getPatient()
						.getAdministrativeGenderCode().getCode()));
				person.setGenderSourceValue(patientRole.getPatient()
						.getAdministrativeGenderCode().getDisplayName());

				if (patientRole.getPatient().getAdministrativeGenderCode()
						.getNullFlavor() != null) {
					person.setGenderCdNull(patientRole.getPatient()
							.getAdministrativeGenderCode().getNullFlavor()
							.getName()); // @todo - check
				}
			}
			// if (patientRole.getPatient().getMaritalStatusCode() != null) {
			// person.setMaritalStatusCode(getCode(patientRole.getPatient().getMaritalStatusCode()));
			// }
			if (patientRole.getPatient().getRaceCode() != null) {
				person.setRaceCdValue(getCodeValue(patientRole.getPatient()
						.getRaceCode().getCode()));
				person.setRaceCdSys(getVocabSys(patientRole.getPatient()
						.getRaceCode().getCodeSystem()));
				person.setRaceCdSysName(patientRole.getPatient().getRaceCode()
						.getCodeSystemName());
				person.setRaceSourceValue(patientRole.getPatient()
						.getRaceCode().getDisplayName());
				if (patientRole.getPatient().getRaceCode().getNullFlavor() != null) {
					person.setRaceCdNull(patientRole.getPatient().getRaceCode()
							.getNullFlavor().getName());
				}
			}
			if (patientRole.getPatient().getEthnicGroupCode() != null) {
				person.setEthinicityCdSys(getVocabSys(patientRole.getPatient()
						.getEthnicGroupCode().getCodeSystem()));
				person.setEthinicityCdSysName(patientRole.getPatient()
						.getEthnicGroupCode().getCodeSystemName());
				person.setEthinicityCdValue(getCodeValue(patientRole
						.getPatient().getEthnicGroupCode().getCode()));
				person.setEthinicitySourceValue(patientRole.getPatient()
						.getEthnicGroupCode().getDisplayName());
				if (patientRole.getPatient().getEthnicGroupCode()
						.getNullFlavor() != null) {
					person.setEthinicityCdNull(patientRole.getPatient()
							.getEthnicGroupCode().getNullFlavor().getName());
				}
			}

			if (patientRole.getPatient().getBirthTime() != null) {
				String timeStamp = patientRole.getPatient().getBirthTime()
						.getValue();
				/**
				 * if they only supply yyyyMMdd
				 * add the padding for hhmm
				 */
				if(timeStamp.length()<12){
					timeStamp = timeStamp+"0000";
				}
				DateFormat inputFormat = new SimpleDateFormat("yyyyMMddhhmm");
				try {
					Date dob = new Date(inputFormat.parse(timeStamp).getTime());
					Calendar c = Calendar.getInstance();
					c.setTime(dob);
					person.setDayOfBirth(c.get(Calendar.DAY_OF_MONTH) + "");
					person.setMonthOfBirth((c.get(Calendar.MONTH) + 1) + "");
					person.setYearOfBirth(c.get(Calendar.YEAR) + "");
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

}
