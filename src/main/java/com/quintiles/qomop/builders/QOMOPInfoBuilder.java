package com.quintiles.qomop.builders;

import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quintiles.qomop.main.QOMOP;

/*
 * Main builder takes care of populating the whole QOMOP structure for a given person;
 */
public class QOMOPInfoBuilder extends Builder
{
	private static Logger log = LoggerFactory.getLogger(QOMOPInfoBuilder.class);

	/*
	 * Entry point for building the QOMOP data model
	 */
	public static QOMOP build(ContinuityOfCareDocument ccd)
	{
		QOMOP qomop = null;
		long start = System.currentTimeMillis();
		try
		{
			qomop = new QOMOP();
			PersonInfoBuilder.build(ccd, qomop);
			OrganizationInfoBuilder.build(ccd, qomop);
			ProviderInfoBuilder.build(ccd, qomop);
			VisitOccurrenceInfoBuilder.build(ccd, qomop);
			ObservationInfoBuilder.build(ccd, qomop);
			ProcedureOccurrenceInfoBuilder.build(ccd, qomop);
			DrugExposureInfoBuilder.build(ccd, qomop);			
			ConditionOccurrenceInfoBuilder.build(ccd, qomop);
			
		}
		catch (Exception e)
		{			// TODO Auto-generated catch block
			e.printStackTrace();
			if(ccd!=null && ccd.getId()!=null)
			System.err.println("For DocumentId:"+ccd.getId().getExtension()+" error message:"+e.getMessage());
		}
		/*finally{
			if(ccd!=null && ccd.getId()!=null)
			System.out.println("Processing time for DocumentID:"+ccd.getId().getExtension()+" is "+(System.currentTimeMillis()-start)+" milli secs");
		}*/
        
		return qomop;
	}
}
