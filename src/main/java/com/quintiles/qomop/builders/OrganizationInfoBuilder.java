package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.ON;

import com.quintiles.omop.Location;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class OrganizationInfoBuilder extends Builder
{

    public static void build(ContinuityOfCareDocument ccd, QOMOP qomop)
    {
        List<com.quintiles.omop.Organization> organizationList = new ArrayList<com.quintiles.omop.Organization>();
        String careSiteId = null;
        String sourceMemberId = null;
        String organizationName = null;
        
        EList<PatientRole> patientRoles = ccd.getPatientRoles();
        if ((patientRoles != null) && (patientRoles.size() > 0)) {
            for (PatientRole patientRole : patientRoles) {

                org.openhealthtools.mdht.uml.cda.Organization org = patientRole.getProviderOrganization();
                if (org != null && isValidEntry(org.getNullFlavor())) {
                    com.quintiles.omop.Organization omopOrg = new com.quintiles.omop.Organization();
                    omopOrg.setPersonId(OMOPUtils.getPersonId(ccd));
                    for (II id : org.getIds()) {
                        if (id.getRoot().equals("2.16.840.1.113883.19.3")) {
                            careSiteId = getExtension(id);
                        }
                        if (id.getRoot().equals("2.16.840.1.113883.19.1")) {
                            sourceMemberId = getExtension(id);
                        }
                    }
                    
                    for (ON on : org.getNames()){
                    	organizationName = on.getText();
                    	break;
                    }
                    omopOrg.setMemberId(sourceMemberId);
                    omopOrg.setSourceMemberId(sourceMemberId);
                    omopOrg.setSourceKey(omopOrg.getMemberId() + ":" + careSiteId);  //@todo: this may need to change
                    String hashValue = OMOPUtils.getHashValue(omopOrg.getSourceKey());
					omopOrg.setOrganizationId(hashValue);
                    
                    omopOrg.setOrganizationSourceValue(organizationName);
                     
                    omopOrg.setPosCdSys("UNK_VOC");
                    omopOrg.setPosCdValue("UNK_CD");
                    
                    
                    EList<AD> addresses = org.getAddrs();
                    if (addresses.size() > 0) {
                        Location loc = OMOPUtils.getLocation(addresses.get(0),ccd);                     	
                        qomop.getLocationList().add(loc);                        
                        //omopOrg.setLocationId(loc.getLocationId());
                        omopOrg.setLocationSid(loc.getLocationSid());
                    }

                    omopOrg.setCreateDate(getFormattedDate(ccd.getEffectiveTime().getValue()));

/*        @todo
                    if (org.getNames() != null && org.getNames().size() > 0) {
                        pOrganization.setName(org.getNames().get(0).getText());
                    }
*/

			/*
			 * Must have some code associated with pOrganization
			 */
                    organizationList.add(omopOrg);
                }
            }
        }
        qomop.setOrganizationList(organizationList);
    }
}