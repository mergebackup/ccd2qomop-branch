package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.AssignedEntity;
import org.openhealthtools.mdht.uml.cda.DocumentationOf;
import org.openhealthtools.mdht.uml.cda.Performer1;
import org.openhealthtools.mdht.uml.cda.ServiceEvent;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;

import com.quintiles.omop.Provider;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class ProviderInfoBuilder extends Builder {
	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {
		List<Provider> providerList = new ArrayList<Provider>();
		handleProvider(ccd, providerList);
		qomop.setProviderList(providerList);

	}

	public static void handleProvider(ContinuityOfCareDocument ccd,
			List<Provider> providerList) {
		EList<DocumentationOf> documents = ccd.getDocumentationOfs();
		for (DocumentationOf doc : documents) {
			ServiceEvent sEvent = doc.getServiceEvent();
			if (sEvent != null && sEvent.getPerformers() != null
					&& sEvent.getPerformers().size() > 0) {
				Performer1 provider = sEvent.getPerformers().get(0);
				if (isValidEntry(provider.getNullFlavor())) {
					Provider qProv = new Provider();
					qProv.setSourceKey(extractProviderId(provider));
					qProv.setProviderId(OMOPUtils.getHashValue(qProv.getSourceKey()));
					
					qProv.setCreateDate(Builder.getFormattedDate((ccd.getEffectiveTime().getValue())));
					qProv.setPersonId(OMOPUtils.getPersonId(ccd));
					qProv.setSourceMemberId(getMemberId());

					if (provider.getAssignedEntity() != null
							&& isValidEntry(provider.getAssignedEntity()
									.getNullFlavor())) {
						if (provider.getAssignedEntity().getCode() != null) {

							if (provider.getAssignedEntity().getCode()
									.getNullFlavor() != null) {
								qProv.setSpecialtyCdNull(provider
										.getAssignedEntity().getCode()
										.getNullFlavor().getName());
							}
							qProv.setSpecialtyCdSys(getVocabSys(provider
									.getAssignedEntity().getCode()
									.getCodeSystem()));
							qProv.setSpecialtyCdSysName(provider
									.getAssignedEntity().getCode()
									.getCodeSystemName());
							qProv.setSpecialtyCdValue(getCodeValue(provider
									.getAssignedEntity().getCode().getCode()));
							qProv.setSpecialtySourceValue(provider
									.getAssignedEntity().getCode()
									.getDisplayName());

						}
						if (provider.getAssignedEntity().getAssignedPerson() != null) {

							qProv.setProviderSourceValue(getProviderSourceValue(provider
									.getAssignedEntity()));
						}

					}
					providerList.add(qProv);
				}
			}
		}
	}

	public static String getProviderSourceValue(AssignedEntity asgnEntity) {
		if (asgnEntity.getAssignedPerson().getNames() != null
				&& asgnEntity.getAssignedPerson().getNames().size() > 0) {
			if (asgnEntity.getAssignedPerson().getNames().get(0).getGivens() != null) {

				String firstName = "";
				String lastName = "";
				if (asgnEntity.getAssignedPerson().getNames().get(0)
						.getGivens().size() > 0
						&& asgnEntity.getAssignedPerson().getNames().get(0)
								.getGivens().get(0) != null) {
					firstName = asgnEntity.getAssignedPerson().getNames()
							.get(0).getGivens().get(0).getText();
				}
				if (asgnEntity.getAssignedPerson().getNames().get(0)
						.getFamilies().size() > 0
						&& asgnEntity.getAssignedPerson().getNames().get(0)
								.getFamilies().get(0) != null) {
					lastName = asgnEntity.getAssignedPerson().getNames().get(0)
							.getFamilies().get(0).getText();
				}

				return firstName + " " + lastName;
			}
		}
		return null;
	}

	public static String extractProviderId(Performer1 provider) {
		if (provider.getAssignedEntity() != null
				&& provider.getAssignedEntity().getIds().size() > 0) {
			Pattern pattern = Pattern.compile(PROV_ROOT_PATTRN);
			for (II id : provider.getAssignedEntity().getIds()) {
				if (id.getRoot() != null) {
					Matcher matcher = pattern.matcher(id.getRoot());
					if (matcher.matches()) {
						/*
						 * Provider Id is within extension no need to
						 * concatenate the root(doing otherwise we cant identify
						 * uniquely a provider).
						 */
						return id.getExtension();
					}
				}

			}

		}
		return null;
	}
}
