package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.Participant2;
import org.openhealthtools.mdht.uml.cda.ParticipantRole;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityAct;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityObservation;
import org.openhealthtools.mdht.uml.cda.consol.ProcedureActivityProcedure;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;

import com.quintiles.omop.ProcedureOccurrence;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class ProcedureOccurrenceInfoBuilder extends Builder {
	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {
		List<com.quintiles.omop.ProcedureOccurrence> procedureList = new ArrayList<com.quintiles.omop.ProcedureOccurrence>();

		/*
		 * Iterating through procedures which represents new information about
		 * patient with physical alteration
		 */
		EList<ProcedureActivityProcedure> proceduresWithAlteration = ccd
				.getProceduresSection().getProcedureActivityProcedures();
		/*
		 * Iterating through procedures which represents new information about
		 * patient without physical alteration
		 */
		EList<ProcedureActivityObservation> proceduresWithNoAlteration = ccd
				.getProceduresSection().getProcedureActivityObservations();
		/*
		 * Iterating through procedures which represents all other types of
		 * procedures
		 */
		EList<ProcedureActivityAct> proceduresOtherType = ccd
				.getProceduresSection().getProcedureActivityActs();

		getPatientProviderOrganization(ccd.getPatientRoles());
		
		buildProceduresActivityProcedure(ccd, proceduresWithAlteration,
				procedureList);
		buildProceduresActivityObservation(ccd, proceduresWithNoAlteration,
				procedureList);
		buildProceduresActivityAct(ccd, proceduresOtherType, procedureList);

		/*
		 * procSection.setProcedureList(procedureList);
		 * lcd.setProcedureSection(procSection);
		 */
		qomop.setProcedureOcclist(procedureList);
	}

	

	public static void buildProceduresActivityAct(ContinuityOfCareDocument ccd,
			EList<ProcedureActivityAct> procedureActivities,
			List<ProcedureOccurrence> procedureList) {

		/*
		 * Iterating through procedures which represents all other types of
		 * procedures
		 */
		for (ProcedureActivityAct paa : procedureActivities) {
			if (isValidEntry(paa.getNullFlavor())) {
				com.quintiles.omop.ProcedureOccurrence proc = new com.quintiles.omop.ProcedureOccurrence();
				proc.setPersonId(OMOPUtils.getPersonId(ccd));
				proc.setSourceMemberId(Builder.getMemberId());
				if (paa.getAuthors() != null && paa.getAuthors().size() > 0) {
					proc.setSourceKey(OMOPUtils.getId(paa.getIds().get(0), paa
							.getAuthors().get(0).getTime()));
					//proc.setProcedureOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
				} else {
					proc.setSourceKey(OMOPUtils.getId(paa.getIds().get(0)));
                  }
                   
					String hashValue = OMOPUtils.getHashValue(proc.getSourceKey());
					proc.setProcedureOccurrenceId(hashValue);
					
					
					if (paa.getCode() != null) {
						proc.setProcedureCdValue(getCodeValue(paa.getCode().getCode()));
						proc.setProcedureCdSys(getVocabSys(paa.getCode().getCodeSystem()));
						proc.setProcedureCdSysName(paa.getCode()
								.getCodeSystemName());
						proc.setProcedureSourceValue(paa.getCode()
								.getDisplayName());
						if (paa.getCode().getNullFlavor() != null) {
							proc.setProcedureCdNull(paa.getCode()
									.getNullFlavor().getName());
						}
					
					if ((paa.getEffectiveTime() != null)
							&& (paa.getEffectiveTime().getLow() != null)) {
						proc.setProcedureDate(Builder.getFormattedDate(paa
								.getEffectiveTime().getLow().getValue()));
						proc.setProcedureEndDate(Builder.getFormattedDate(paa
								.getEffectiveTime().getHigh().getValue()));
					}

					handleServiceDelLocations(paa.getParticipants(), proc);

					if (paa.getIndications().size() > 0) {
						if (paa.getIndications().get(0).getIds().size() > 0) {
							proc.setRelevantConditionConceptId(OMOPUtils
									.getId(paa.getIndications().get(0).getIds()
											.get(0)));
						}
					}

					getPerformerInfo(paa.getPerformers(), proc);
					handleEntryRelationShips(proc, paa.getEntryRelationships());
					proc.setCreateDate(ccd.getEffectiveTime().getValue());

					procedureList.add(proc);
				}
			}
		}
	}

	public static void buildProceduresActivityObservation(
			ContinuityOfCareDocument ccd,
			EList<ProcedureActivityObservation> procedureActivities,
			List<ProcedureOccurrence> procedureList) {

		/*
		 * Iterating through procedures which represents all other types of
		 * procedures
		 */
		for (ProcedureActivityObservation pao : procedureActivities) {
			if (isValidEntry(pao.getNullFlavor())) {
				com.quintiles.omop.ProcedureOccurrence proc = new com.quintiles.omop.ProcedureOccurrence();
				proc.setSourceMemberId(Builder.getMemberId());
				proc.setPersonId(OMOPUtils.getPersonId(ccd));
				if (pao.getAuthors() != null && pao.getAuthors().size() > 0) {
					proc.setSourceKey(OMOPUtils.getId(pao.getIds().get(0), pao
							.getAuthors().get(0).getTime()));
					//proc.setProcedureOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
				} else {
					proc.setSourceKey(OMOPUtils.getId(pao.getIds().get(0)));
					//proc.setProcedureOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
				}
                
				String hashValue = OMOPUtils.getHashValue(proc.getSourceKey());
				proc.setProcedureOccurrenceId(hashValue);
				
				if (pao.getCode() != null) {
					proc.setProcedureCdValue(getCodeValue(pao.getCode().getCode()));
					proc.setProcedureCdSys(getVocabSys(pao.getCode().getCodeSystem()));
					proc.setProcedureCdSysName(pao.getCode()
							.getCodeSystemName());
					proc.setProcedureSourceValue(pao.getCode().getDisplayName());
					if (pao.getCode().getNullFlavor() != null) {
						proc.setProcedureCdNull(pao.getCode().getNullFlavor()
								.getName());
					}
				}

				if ((pao.getEffectiveTime() != null)
						&& (pao.getEffectiveTime().getLow() != null)) {
					proc.setProcedureDate(Builder.getFormattedDate(pao
							.getEffectiveTime().getLow().getValue()));
					proc.setProcedureEndDate(Builder.getFormattedDate(pao
							.getEffectiveTime().getHigh().getValue()));
				}

				handleServiceDelLocations(pao.getParticipants(), proc);

				if (pao.getIndications().size() > 0) {
					if (pao.getIndications().get(0).getIds().size() > 0) {
						proc.setRelevantConditionConceptId(OMOPUtils.getId(pao
								.getIndications().get(0).getIds().get(0)));
					}
				}

				getPerformerInfo(pao.getPerformers(), proc);
				handleEntryRelationShips(proc, pao.getEntryRelationships());
				proc.setCreateDate(ccd.getEffectiveTime().getValue());
				procedureList.add(proc);
			}
		}
	}

	public static void buildProceduresActivityProcedure(
			ContinuityOfCareDocument ccd,
			EList<ProcedureActivityProcedure> procedureActivities,
			List<ProcedureOccurrence> procedureList) {

		/*
		 * Iterating through procedures which represents all other types of
		 * procedures
		 */
		for (ProcedureActivityProcedure pap : procedureActivities) {
			if (isValidEntry(pap.getNullFlavor())) {
				com.quintiles.omop.ProcedureOccurrence proc = new com.quintiles.omop.ProcedureOccurrence();
				proc.setSourceMemberId(Builder.getMemberId());
				proc.setPersonId(OMOPUtils.getPersonId(ccd));
				if (pap.getAuthors() != null && pap.getAuthors().size() > 0) {
					proc.setSourceKey(OMOPUtils.getId(pap.getIds().get(0), pap
							.getAuthors().get(0).getTime()));
					//proc.setProcedureOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
				} else {
					proc.setSourceKey(OMOPUtils.getId(pap.getIds().get(0)));
					//proc.setProcedureOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
				}

				String hashValue = OMOPUtils.getHashValue(proc.getSourceKey());
				proc.setProcedureOccurrenceId(hashValue);
				
				if (pap.getCode() != null) {
					proc.setProcedureCdValue(getCodeValue(pap.getCode().getCode()));
					proc.setProcedureCdSys(getVocabSys(pap.getCode().getCodeSystem()));
					proc.setProcedureCdSysName(pap.getCode()
							.getCodeSystemName());
					proc.setProcedureSourceValue(pap.getCode().getDisplayName());
					if (pap.getCode().getNullFlavor() != null) {
						proc.setProcedureCdNull(pap.getCode().getNullFlavor()
								.getName());
					}
				}

				if ((pap.getEffectiveTime() != null)
						&& (pap.getEffectiveTime().getLow() != null)) {
					proc.setProcedureDate(Builder.getFormattedDate(pap
							.getEffectiveTime().getLow().getValue()));
					proc.setProcedureEndDate(Builder.getFormattedDate(pap
							.getEffectiveTime().getHigh().getValue()));
				}

				handleServiceDelLocations(pap.getParticipants(), proc);

				if (pap.getIndications().size() > 0) {
					if (pap.getIndications().get(0).getIds().size() > 0) {
						proc.setRelevantConditionConceptId(OMOPUtils.getId(pap
								.getIndications().get(0).getIds().get(0)));
					}
				}

				getPerformerInfo(pap.getPerformers(), proc);
				handleEntryRelationShips(proc, pap.getEntryRelationships());
				proc.setCreateDate(ccd.getEffectiveTime().getValue());
				procedureList.add(proc);
			}
		}
	}

	public static void handleServiceDelLocations(
			EList<Participant2> partcpntList,
			com.quintiles.omop.ProcedureOccurrence proc) {

		for (Participant2 prtnt : partcpntList) {
			ParticipantRole pRole = prtnt.getParticipantRole();
			if (pRole != null && pRole.getTemplateIds().size() > 0) {
				if (pRole.getTemplateIds().get(0).getRoot()
						.equals("2.16.840.1.113883.10.20.22.4.32")) {
					proc.setCaresiteId(OMOPUtils.getId(pRole.getTemplateIds()
							.get(0)));
				}
			}
		}
	}

	/*
	 * Get Perfomer Info for the Procedure
	 */
	public static void getPerformerInfo(final EList<Performer2> performers,
			final com.quintiles.omop.ProcedureOccurrence proc) {
		if (performers.size() > 0) {
			Performer2 performer = performers.get(0);
			proc.setAssociatedProviderId(extractProviderId(performer));
		}
	}

	/*
	 * public static String getPerformerID(final Performer2 performer) { if
	 * (performer != null) { AssignedEntity asgnEntity =
	 * performer.getAssignedEntity();
	 * 
	 * for (II id : asgnEntity.getIds()) { if (id != null && id.getRoot() !=
	 * null) { if (id.getRoot().substring(id.getRoot().length() - 3,
	 * id.getRoot().length()).equals("2.2")) { return OMOPUtils.getId(id); } } }
	 * 
	 * } return null; }
	 */

	public static void handleEntryRelationShips(
			final com.quintiles.omop.ProcedureOccurrence proc,
			final EList<EntryRelationship> entryRelationShips) {
		/*
		 * Iterating through entryRelationShips to find the encounter and
		 * medicationActivity associated with the procedure
		 */
		for (EntryRelationship eRS : entryRelationShips) {

			if (eRS.getEncounter() != null) {
				proc.setVisitOccurrenceId(getId(eRS.getEncounter().getIds()
						.get(0)));
				break;
			}

		}
		return;
	}

}
