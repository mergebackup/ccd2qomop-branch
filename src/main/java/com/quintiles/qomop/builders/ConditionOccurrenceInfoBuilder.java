package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.Participant2;
import org.openhealthtools.mdht.uml.cda.consol.AllergyObservation;
import org.openhealthtools.mdht.uml.cda.consol.AllergyProblemAct;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.consol.ProblemConcernAct;
import org.openhealthtools.mdht.uml.cda.consol.ProblemObservation;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;

import com.quintiles.omop.ConditionOccurrence;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class ConditionOccurrenceInfoBuilder extends Builder {

	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {
		List<ConditionOccurrence> conditionOccList = new ArrayList<ConditionOccurrence>();
		handleAllergySection(ccd, conditionOccList);
		handleProblemSection(ccd, conditionOccList);
		qomop.setConditionOccList(conditionOccList);

	}

	public static void handleAllergySection(ContinuityOfCareDocument ccd,
			List<ConditionOccurrence> conditionOccList) {
		if (ccd.getProblemSection() != null
				&& ccd.getProblemSection().getProblemConcerns() != null
				&& ccd.getProblemSection().getProblemConcerns().size() > 0) {
			EList<AllergyProblemAct> allergyProblemActs = ccd
					.getAllergiesSection().getAllergyProblemActs();

			for (AllergyProblemAct act : allergyProblemActs) {
				for (AllergyObservation algyOb : act.getAllergyObservations()) {
					if (isValidEntry(algyOb.getNullFlavor())) {

						ConditionOccurrence qCondition = new ConditionOccurrence();
						//qCondition.setConditionOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
						qCondition.setCreateDate(Builder.getFormattedDate((ccd
								.getEffectiveTime().getValue())));
						qCondition
								.setSourceMemberId(OMOPUtils.getMemberId(ccd));
						qCondition.setPersonId(OMOPUtils.getPersonId(ccd));

						if (algyOb.getEffectiveTime() != null) {
							if (algyOb.getEffectiveTime().getHigh() != null
									&& !algyOb.getEffectiveTime().getHigh()
											.isSetNullFlavor()) {
								qCondition
										.setConditionEndDate(getFormattedDate(algyOb
												.getEffectiveTime().getHigh()
												.getValue()));
							}
							if (algyOb.getEffectiveTime().getLow() != null
									&& !algyOb.getEffectiveTime().getLow()
											.isSetNullFlavor()) {
								qCondition
										.setConditionStartDate(getFormattedDate(algyOb
												.getEffectiveTime().getLow()
												.getValue()));
							}
						}
						/*
						 * This code tells us about Allergy/Adverse Event
						 * Type(Value Set: 2.16.840.1.113883.3.88.12.3221.7.4 )
						 */
						if (algyOb.getValues().size() > 0
								&& (algyOb.getValues().get(0) instanceof CD)) {
							CD valueCode = (CD) algyOb.getValues().get(0);
							qCondition.setConditionTypeCdDisplay(valueCode
									.getDisplayName());
							qCondition.setConditionTypeCdSys(getVocabSys(valueCode.getCodeSystem()));
							qCondition.setConditionTypeCdName(valueCode.getCodeSystemName());
							qCondition.setConditionTypeCdValue(getCodeValue(valueCode.getCode()));
							if (valueCode.getNullFlavor() != null) {
								qCondition.setConditionTypeCdNull(valueCode
										.getNullFlavor().getName());
							}
						}

						/*
						 * This code tell us about Allergy due to from these
						 * (ValueSet : MedicationClinical Drug,Medication Drug
						 * clas,Food or substance Name)
						 */
						if (algyOb.getParticipants().size() > 0) {
							Participant2 participant = algyOb.getParticipants()
									.get(0);
							if (participant.getParticipantRole() != null
									&& participant.getParticipantRole()
											.getPlayingEntity() != null) {
								CE participantCode = participant
										.getParticipantRole()
										.getPlayingEntity().getCode();
								if (participantCode != null) {
									qCondition
											.setConditionCdDisplay(participantCode
													.getDisplayName());
									qCondition
											.setConditionCdSys(getVocabSys(participantCode
													.getCodeSystem()));
									qCondition
											.setConditionCdSysName(participantCode
													.getCodeSystemName());
									qCondition
											.setConditionCdValue(getCodeValue(participantCode
													.getCode()));
									if (participantCode.getNullFlavor() != null) {
										qCondition
												.setConditionCdNull(participantCode
														.getNullFlavor()
														.getName());
									}
								}
							}
						}

						setAllergySourceKey(algyOb, qCondition);
						String hashValue = OMOPUtils.getHashValue(qCondition.getSourceKey());
						qCondition.setConditionOccurrenceId(hashValue);
						/*
						 * Extract associated providerId if exists.
						 */
						if (algyOb.getPerformers().size() > 0) {
							qCondition
									.setAssociatedProviderId(extractProviderId(algyOb
											.getPerformers().get(0)));
						}

						conditionOccList.add(qCondition);
					}
				}
			}
		}
	}

	public static void handleProblemSection(ContinuityOfCareDocument ccd,
			List<ConditionOccurrence> conditionOccList) {

		if (ccd.getProblemSection() != null
				&& ccd.getProblemSection().getProblemConcerns() != null
				&& ccd.getProblemSection().getProblemConcerns().size() > 0) {

			EList<ProblemConcernAct> problemConcernActs = ccd
					.getProblemSection().getProblemConcerns();
			for (ProblemConcernAct act : problemConcernActs) {
				/*
				 * Getting the ProblemObservation observation under Problem
				 * Concern Act
				 */
				for (ProblemObservation probOb : act.getProblemObservations()) {
					if (isValidEntry(probOb.getNullFlavor())) {
						ConditionOccurrence qCondition = new ConditionOccurrence();
						//qCondition.setConditionOccurrenceId(Long.toString(OMOPUtils.generateUniqueID()));
						qCondition.setCreateDate(Builder.getFormattedDate((ccd
								.getEffectiveTime().getValue())));
						qCondition.setPersonId(OMOPUtils.getPersonId(ccd));
						qCondition
								.setSourceMemberId(OMOPUtils.getMemberId(ccd));

						if (probOb.getEffectiveTime() != null) {
							if (probOb.getEffectiveTime().getHigh() != null
									&& !probOb.getEffectiveTime().getHigh()
											.isSetNullFlavor()) {
								qCondition
										.setConditionEndDate(getFormattedDate(probOb
												.getEffectiveTime().getHigh()
												.getValue()));
							}
							if (probOb.getEffectiveTime().getLow() != null
									&& !probOb.getEffectiveTime().getHigh()
											.isSetNullFlavor()) {
								qCondition
										.setConditionStartDate(getFormattedDate(probOb
												.getEffectiveTime().getLow()
												.getValue()));
							}
						}
						/*
						 * This code tells us about ProblemValue(Value Set:
						 * 2.16.840.1.113883.3.88.12.3221.7.4 )
						 */
						if (probOb.getValues().size() > 0
								&& (probOb.getValues().get(0) instanceof CD)) {
							CD valueCode = (CD) probOb.getValues().get(0);
							qCondition.setConditionCdDisplay(valueCode
									.getDisplayName());
							qCondition.setConditionCdSys(getVocabSys(valueCode
									.getCodeSystem()));
							qCondition.setConditionCdSysName(valueCode
									.getCodeSystemName());
							qCondition.setConditionCdValue(getCodeValue(valueCode.getCode()));
							if (valueCode.getNullFlavor() != null) {
								qCondition
										.setConditionCdNull(valueCode
												.getNullFlavor()
												.getName());
							}
						}
						/*
						 * This code tells us about ProblemType(Value Set:
						 * 2.16.840.1.113883.3.88.12.3221.7.2 )
						 */
						if (probOb.getCode() != null) {
							qCondition.setConditionTypeCdDisplay(probOb
									.getCode().getDisplayName());
							qCondition.setConditionTypeCdName(probOb.getCode()
									.getCodeSystemName());
							qCondition.setConditionTypeCdNull(probOb.getCode()
									.getNullFlavor().toString());
							qCondition.setConditionTypeCdSys(getVocabSys(probOb.getCode()
									.getCodeSystem()));
							qCondition.setConditionTypeCdValue(getCodeValue(probOb.getCode()
									.getCode()));
						}

						setProblemSourceKey(probOb, qCondition);
						String hashValue = OMOPUtils.getHashValue(qCondition.getSourceKey());
						qCondition.setConditionOccurrenceId(hashValue);
						/*
						 * Extract associated providerId if exists.
						 */
						if (probOb.getPerformers().size() > 0) {
							qCondition
									.setAssociatedProviderId(extractProviderId(probOb
											.getPerformers().get(0)));
						}

						conditionOccList.add(qCondition);
					}
				}
			}
		}
	}

	public static void setAllergySourceKey(AllergyObservation algyOb,
			ConditionOccurrence qCondition) {
		String authoredTime = "";
		if (algyOb.getAuthors().size() > 0) {
			if (algyOb.getAuthors().get(0).getTime() != null) {
				authoredTime = algyOb.getAuthors().get(0).getTime().getValue();
			}
		}

		/*
		 * handle sourckey which is combination of root,extension and
		 * authoredTime
		 */
		if (algyOb.getIds().size() > 0) {
			II id = algyOb.getIds().get(0);
			String sourceKey = OMOPUtils.getId(id, authoredTime);
			qCondition.setSourceKey(sourceKey);
		}
	}

	public static void setProblemSourceKey(ProblemObservation probOb,
			ConditionOccurrence qCondition) {
		String authoredTime = "";
		if (probOb.getAuthors().size() > 0) {
			if (probOb.getAuthors().get(0).getTime() != null) {
				authoredTime = probOb.getAuthors().get(0).getTime().getValue();
			}
		}

		/*
		 * handle Id which is combination of root,extension and authored time
		 */
		if (probOb.getIds().size() > 0) {
			II id = probOb.getIds().get(0);
			qCondition.setSourceKey(OMOPUtils.getId(id, authoredTime));

		}
	}

}
