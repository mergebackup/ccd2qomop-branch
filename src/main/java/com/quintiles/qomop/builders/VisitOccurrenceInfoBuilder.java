package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.openhealthtools.mdht.uml.cda.ParticipantRole;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.cda.consol.EncounterActivities;

import com.quintiles.omop.CareSite;
import com.quintiles.omop.Location;
import com.quintiles.omop.VisitOccurrence;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class VisitOccurrenceInfoBuilder extends Builder {
	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {
		handleVisitOccurrence(ccd, qomop);
	}

	public static void handleVisitOccurrence(ContinuityOfCareDocument ccd,
			QOMOP qomop) {

		List<com.quintiles.omop.VisitOccurrence> visitOccurrences = new ArrayList<com.quintiles.omop.VisitOccurrence>();
		if (ccd.getEncountersSection() != null) {
			for (EncounterActivities ccdEncounterActivity : ccd
					.getEncountersSection().getEncounterActivitiess()) {

				if (isValidEntry(ccdEncounterActivity.getNullFlavor())) {
					com.quintiles.omop.VisitOccurrence qVisit = new com.quintiles.omop.VisitOccurrence();
					if (ccdEncounterActivity.getIds().size() > 0) {
						qVisit.setSourceKey(OMOPUtils.getId(ccdEncounterActivity.getIds().get(0)));
						qVisit.setVisitOccurrenceId(OMOPUtils.getHashValue(qVisit.getSourceKey()));
					}
					if (ccdEncounterActivity.getCode() != null) {
						qVisit.setVisitCdValue(getCodeValue(ccdEncounterActivity
								.getCode().getCode()));
						qVisit.setVisitCdSys(getVocabSys(ccdEncounterActivity
								.getCode().getCodeSystem()));
						qVisit.setVisitCdSysName(ccdEncounterActivity.getCode()
								.getCodeSystemName());
						qVisit.setPlaceOfServiceSourceValue(ccdEncounterActivity
								.getCode().getDisplayName());
						if (ccdEncounterActivity.getCode().getNullFlavor() != null) {
							qVisit.setVisitCdNull(ccdEncounterActivity
									.getCode().getNullFlavor().getName());
						}
					}
					/*
					 * Capturing the duration of the encounter
					 */
					captureDuration(ccdEncounterActivity, qVisit);

					/*
					 * ***************Service Delivery Location
					 * Information**********************
					 */

					if (ccdEncounterActivity.getParticipants().size() > 0) {
						handleCareSite(ccdEncounterActivity, qVisit, ccd, qomop);
					}

					qVisit.setCreateDate(Builder.getFormattedDate((ccd
							.getEffectiveTime().getValue())));

					qVisit.setPersonId(OMOPUtils.getPersonId(ccd));

					getPatientProviderOrganization(ccd.getPatientRoles());

					qVisit.setSourceMemberId(Builder.getMemberId());

					// @todo: where to get sourceMemberId
					visitOccurrences.add(qVisit);
				}
			}

		}

		qomop.setVisitOccList(visitOccurrences);

	}

	public static void handleCareSite(EncounterActivities ccdEncounterActivity,
			VisitOccurrence qVisit, ContinuityOfCareDocument ccd, QOMOP qomop) {
		ParticipantRole pRole = ccdEncounterActivity.getParticipants().get(0)
				.getParticipantRole();
		if (pRole != null && isValidEntry(pRole.getNullFlavor())) {
			CareSite qSite = new CareSite();
			qSite.setCreateDate(Builder.getFormattedDate((ccd
					.getEffectiveTime().getValue())));
			qSite.setPersonId(OMOPUtils.getPersonId(ccd));

			if (pRole.getIds().size() > 0) {
				qSite.setSourceKey(OMOPUtils.getId(pRole.getIds().get(0)));
				qSite.setCareSiteId(OMOPUtils.getHashValue(qSite.getSourceKey()));
				qVisit.setCareSiteId(qSite.getCareSiteId());
				//qVisit.setCareSiteId(OMOPUtils.getId(pRole.getIds().get(0)));
			}
			if (pRole.getCode() != null) {
				qSite.setPlaceOfServiceSourceValue(pRole.getCode()
						.getDisplayName());
				qSite.setPosCdDisplay(pRole.getCode().getDisplayName());
				if (pRole.getCode().getNullFlavor() != null) {
					qSite.setPosCdNull(pRole.getCode().getNullFlavor()
							.getName());
				}
				qSite.setPosCdSys(getVocabSys(pRole.getCode().getCodeSystem()));
				qSite.setPosCdSysName(pRole.getCode().getCodeSystemName());
				qSite.setPosCdValue(getCodeValue(pRole.getCode().getCode()));
			}else{
				qSite.setPosCdValue("UNK_CD");
				qSite.setPosCdSys("UNK_VOC");
			}
			/*
			 * Handling location associated with a caresite and referencing it
			 * back.
			 */
			if (pRole.getAddrs().size() > 0) {
				Location loc = OMOPUtils.getLocation(pRole.getAddrs().get(0),ccd);
				qSite.setLocationSid(loc.getLocationSid());
				qomop.getLocationList().add(loc);
			}
			qomop.getCareSiteList().add(qSite);
		}
	}

	private static void captureDuration(
			EncounterActivities ccdEncounterActivity,
			com.quintiles.omop.VisitOccurrence visitOccurrence) {
		if (ccdEncounterActivity.getEffectiveTime() != null) {
			if (ccdEncounterActivity.getEffectiveTime().getLow() != null) {
				visitOccurrence
						.setVisitStartDate(getFormattedDate(ccdEncounterActivity
								.getEffectiveTime().getLow().getValue()));
			}
			if (ccdEncounterActivity.getEffectiveTime().getHigh() != null) {
				visitOccurrence
						.setVisitEndDate(getFormattedDate(ccdEncounterActivity
								.getEffectiveTime().getHigh().getValue()));
			}
		}
	}

}
