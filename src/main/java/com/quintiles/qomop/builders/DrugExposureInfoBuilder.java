package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.EntryRelationship;
import org.openhealthtools.mdht.uml.cda.Material;
import org.openhealthtools.mdht.uml.cda.SubstanceAdministration;
import org.openhealthtools.mdht.uml.cda.Supply;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.CE;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.SXCM_TS;
import org.openhealthtools.mdht.uml.hl7.datatypes.impl.IVL_TSImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quintiles.omop.DrugExposure;
import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class DrugExposureInfoBuilder extends Builder
{

	private static Logger log = LoggerFactory.getLogger(DrugExposureInfoBuilder.class);

	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop)
	{

		List<DrugExposure> drugExpList = new ArrayList<DrugExposure>();
		
		handleMedicationSection(ccd, drugExpList);
		handleImmunizationSection(ccd, drugExpList);
		
		qomop.setDrugExpList(drugExpList);

	}

	public static void handleImmunizationSection(ContinuityOfCareDocument ccd, List<DrugExposure> drugExpList)
	{
		if (ccd.getImmunizationsSectionEntriesOptional() != null)
		{
			handleSubAdministrations(drugExpList,ccd, ccd.getImmunizationsSectionEntriesOptional()
					.getSubstanceAdministrations());
		}
	}

	public static void handleMedicationSection(ContinuityOfCareDocument ccd, List<DrugExposure> drugExpList)
	{
		if (ccd.getMedicationsSection() != null)
		{
			handleSubAdministrations(drugExpList,ccd, ccd.getMedicationsSection().getSubstanceAdministrations());
		}
	}

	public static void handleSubAdministrations(List<DrugExposure> drugExpList,ContinuityOfCareDocument ccd,
			EList<SubstanceAdministration> subAdminList)
	{

		if (subAdminList != null)
		{
			for (SubstanceAdministration subAdmin : subAdminList)
			{
				/*
				 * This basically a null check whether the entry has info (If 'NI' it means NoInformation we will skip the
				 * entry)
				 */
				if (isValidEntry(subAdmin.getNullFlavor()))
				{
					DrugExposure qDrug = new DrugExposure();
					
					qDrug.setCreateDate(Builder.getFormattedDate((ccd.getEffectiveTime().getValue())));
					qDrug.setPersonId(OMOPUtils.getPersonId(ccd));
					qDrug.setSourceMemberId(OMOPUtils.getMemberId(ccd));
                   // qDrug.setDrugExposureId(Long.toString(OMOPUtils.generateUniqueID()));					
										
					populateDrugSourceKey(subAdmin, qDrug);
					String hashValue = OMOPUtils.getHashValue(qDrug.getSourceKey());
					qDrug.setDrugExposureId(hashValue);
					/*
					 * Setting the Medication start and end dates
					 */
					EList<SXCM_TS> effectiveTimes = subAdmin.getEffectiveTimes();
					for (SXCM_TS time : effectiveTimes)
					{
						if (time instanceof IVL_TSImpl)
						{
							qDrug.setDrugExposureEndDate(getFormattedDate(((IVL_TSImpl) time).getHigh().getValue()));
							qDrug.setDrugExposureStartDate(getFormattedDate(((IVL_TSImpl) time).getLow().getValue()));
						}
					}

					if (subAdmin.getRepeatNumber() != null && subAdmin.getRepeatNumber().getValue()!=null)
					{
						qDrug.setRefills(subAdmin.getRepeatNumber().getValue().toString());
					}

					
               /*
                * Going against MedicationSupplyOrder to get the medication quantity and prescribing provider Id
                */
					if (subAdmin.getSupplies().size() > 0)
					{
						Supply supply = subAdmin.getSupplies().get(0);
						if(supply.getPerformers()!=null && supply.getPerformers().size()>0){
						  qDrug.setPrescribingProviderId(extractProviderId(supply.getPerformers().get(0)));
						}
						if (supply.getQuantity() != null && supply.getQuantity().getValue() != null)
						{
							qDrug.setQuantity(supply.getQuantity().getValue().toString());
						}

					}
   
					/*
					 * Get the SIG from Instructions act which is under EntryRelationShips.
					 */					
					for(EntryRelationship entryRelation :subAdmin.getEntryRelationships()){
						if(entryRelation.getAct()!=null){
							if(entryRelation.getAct().getTemplateIds().size()>0){
								for(II id : entryRelation.getAct().getTemplateIds()){
									if(id.getRoot().equalsIgnoreCase(INSTRUCTIONS_OID)){
										qDrug.setSig(entryRelation.getAct().getText().getText());
									}
								}
							}
						}
					}
					
					
					/*
					 * Getting the drug code from substance adminsitration
					 */
					if (subAdmin.getConsumable() != null && subAdmin.getConsumable().getManufacturedProduct() != null &&
							subAdmin.getConsumable().getManufacturedProduct().getManufacturedMaterial() != null)
					{
						Material manMaterial = subAdmin.getConsumable().getManufacturedProduct().getManufacturedMaterial();
						CE drugCode = manMaterial.getCode();
						if (drugCode != null)
						{
							qDrug.setDrugCdDisplay(drugCode.getDisplayName());
							qDrug.setDrugCdValue(getCodeValue(drugCode.getCode()));
							qDrug.setDrugCdSys(getVocabSys(drugCode.getCodeSystem()));
							qDrug.setDrugCdSysName(drugCode.getCodeSystemName());
							if(drugCode.getNullFlavor()!=null)
							{
								qDrug.setDrugCdNull(drugCode.getNullFlavor().getName());
							}
							qDrug.setDrugSourceValue(qDrug.getDrugCdDisplay());
						}
					}
					
					drugExpList.add(qDrug);
				}
			}
		}
	}

	

	public static void populateDrugSourceKey(SubstanceAdministration subAdmin, DrugExposure qDrug)
	{
		String authoredTime = "";
		if (subAdmin.getAuthors().size() > 0)
		{
			if (subAdmin.getAuthors().get(0).getTime() != null)
			{
				authoredTime = subAdmin.getAuthors().get(0).getTime().getValue();
			}
		}
		if (subAdmin.getIds().size() > 0)
		{
			II id = subAdmin.getIds().get(0);
			qDrug.setSourceKey(OMOPUtils.getId(id, authoredTime));
			/*if (qDrug.getSourceKey() != null)
			{
				qDrug.setDrugExposureId(qDrug.getSourceKey().hashCode() + "");
			}*/
		}
	}

}
