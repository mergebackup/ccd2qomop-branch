package com.quintiles.qomop.builders;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.Organization;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Performer2;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.vocab.NullFlavor;

public class Builder
{

	public static OrgInfo patientProviderOrg;

	public static String getMemberId()
	{
		if (patientProviderOrg != null)
			return patientProviderOrg.getMemberId();
		else
			return null;
	}

	/*
	 * public static String getProviderId() { if (patientProviderOrg != null) return patientProviderOrg.getProviderId();
	 * else return null; }
	 */

	public static List<String> nullFlavorList = new ArrayList<String>();
	static
	{
		nullFlavorList.add("NI");
//	  nullFlavorList.add("UNK");
	}

	public static final String PATIENT_ROOT_PATTRN = ".*?1\\.3$";
	public static final String PROV_ROOT_PATTRN = ".*?2\\.2$";

	public static final String INSTRUCTIONS_OID = "2.16.840.1.113883.10.20.22.4.20";
	
	public static Map<String,String> observationTypeMap = new HashMap<String,String>();
	static{
		observationTypeMap.put("2.16.840.1.113883.10.20.22.4.27", "Vital Signs");
		observationTypeMap.put("2.16.840.1.113883.10.20.22.4.78", "Social History");
		observationTypeMap.put("2.16.840.1.113883.10.20.22.4.2", "Results");
	}
	
	
	public static String getCodeValue(String s){
		if(s== null || ( s!=null && (s.isEmpty()||s.equals("")) ))
			   return "UNK_CD";
		else
			  return s;
	}
	
	public static String getVocabSys(String s){
		if(s== null || ( s!=null && (s.isEmpty()||s.equals("")) ))
			   return "UNK_VOC";
		else
			  return s;
	}
   
   
	public static boolean isValidEntry(NullFlavor nullFlavor)
	{
		return (nullFlavor == null
		|| (nullFlavor != null && !nullFlavorList.contains(nullFlavor.getName())));
	}

	public static String extractProviderId(Performer2 provider)
	{
		if (isValidEntry(provider.getNullFlavor()))
		{
			if (provider.getAssignedEntity() != null && provider.getAssignedEntity().getIds().size() > 0)
			{
				Pattern pattern = Pattern.compile(PROV_ROOT_PATTRN);
				for (II id : provider.getAssignedEntity().getIds())
				{
					if (id.getRoot() != null)
					{
						Matcher matcher = pattern.matcher(id.getRoot());
						if (matcher.matches())
						{
							/*
							 * Provider Id is within extension no need to concatenate the root(doing otherwise we cant identify
							 * uniquely a provider).
							 */
							return id.getExtension();
						}
					}

				}
			}

		}
		return null;
	}

	public static String getExtension(final II identifier)
	{
		if (identifier != null)
		{
			return identifier.getExtension();
		}
		return null;
	}

	/*public static Calendar getDate(final String effectiveTime)
	{
		if (effectiveTime != null)
		{
			int zone1 = 0;
			int zone2 = 0;

			int year = 0;
			int month = 0;
			int day = 0;
			int hours = 0;
			int minutes = 0;
			int seconds = 0;
			String signOperator = "";

			if (effectiveTime.length() > 3)
			{
				year = Integer.parseInt(effectiveTime.substring(0, 4));
			}
			if (effectiveTime.length() > 5)
			{
				month = Integer.parseInt(effectiveTime.substring(4, 6));
			}
			if (effectiveTime.length() > 7)
			{
				day = Integer.parseInt(effectiveTime.substring(6, 8));
			}
			if (effectiveTime.length() > 9)
			{
				hours = Integer.parseInt(effectiveTime.substring(8, 10));
			}
			if (effectiveTime.length() > 11)
			{
				minutes = Integer.parseInt(effectiveTime.substring(10, 12));
			}
			if (effectiveTime.length() > 13)
			{
				seconds = Integer.parseInt(effectiveTime.substring(12, 14));
			}
			if (effectiveTime.length() > 15)
			{
				signOperator = effectiveTime.substring(14, 15);
			}
			if (effectiveTime.length() > 16)
			{
				zone1 = Integer.parseInt(effectiveTime.substring(14, 17));
			}
			if (effectiveTime.length() > 16)
			{
				zone2 = Integer.parseInt(signOperator + effectiveTime.substring(17, 19));
			}

			Calendar c = new GregorianCalendar();
			c.set(year, month - 1, day, hours, minutes, seconds);
			c.set(Calendar.MILLISECOND, 0);

			// add/subtract hour and minute from time. 

			c.add(Calendar.HOUR, (-1) * zone1);
			c.add(Calendar.MINUTE, (-1) * zone2);

			return c;
		}
		return null;
	}*/

	/*
	 * This method converts the timestamp into GMT specific formatted string
	 */
	public static String getFormattedDate(final String effectiveTime)
	{
		if (effectiveTime != null)
		{
			/*String dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
			Calendar c = getDate(effectiveTime);

			
			 * Properties properties = SettingsManager.properties; if (properties != null){ dateFormat =
			 * properties.getProperty("xml2delimited.dateFormat"); }
			 
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			return sdf.format(c.getTime());*/
			
			DateFormat inputFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");	
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	      try
			{
	      	outputFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
	      	Date d = new Date(inputFormat.parse(effectiveTime).getTime());
				return outputFormat.format(d);
			}
			catch (ParseException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}

		return effectiveTime;
	}

	public static class OrgInfo
	{
		String memberId;
		String careSiteId;

		public OrgInfo(String memberId, String providerId)
		{
			this.memberId = memberId;
			this.careSiteId = providerId;
		}

		public OrgInfo()
		{
		}

		public String getMemberId()
		{
			return memberId;
		}

		public void setMemberId(String memberId)
		{
			this.memberId = memberId;
		}

		public String getCareSiteId()
		{
			return careSiteId;
		}

		public void setCareSiteId(String careSiteId)
		{
			this.careSiteId = careSiteId;
		}

	}

	public static String getId(final II identifier)
	{
		if (identifier != null)
		{
			return identifier.getRoot() + ":" + identifier.getExtension();
		}
		return null;
	}
	
		
	public static void getPatientProviderOrganization(
			EList<PatientRole> patientRoles) {
		if ((patientRoles != null) && (patientRoles.size() > 0)) {
			for (PatientRole patientRole : patientRoles) {
				EList<AD> addrs = patientRole.getAddrs();
				// Only one addrs under PatientRole
				if (addrs.size() > 0) {
					patientProviderOrg = getOrganizationInfo(patientRole);
					break;
				}
			}
		}
	}
	
	public static OrgInfo getOrganizationInfo(PatientRole patientRole)
	{
		Organization org = patientRole.getProviderOrganization();
		OrgInfo pOrganization = new OrgInfo();
		if (org != null)
		{
			/*
			 * Goin through Id's to set the HH-Member Id and HH-Enterprise Id
			 */

			for (II id : org.getIds())
			{
				if (id.getRoot().equals("2.16.840.1.113883.19.2"))
				{
					pOrganization.setMemberId(getExtension(id));
				}
				if (id.getRoot().equals("2.16.840.1.113883.19.3"))
				{
					pOrganization.setCareSiteId(getExtension(id));
				}

			}

		}
		return pOrganization;
	}

}
