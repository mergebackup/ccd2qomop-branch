package com.quintiles.qomop.builders;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.Entry;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.Organizer;
import org.openhealthtools.mdht.uml.cda.ReferenceRange;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.CD;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.PQ;
import org.openhealthtools.mdht.uml.hl7.datatypes.ST;

import com.quintiles.qomop.main.QOMOP;
import com.quintiles.qomop.utility.OMOPUtils;

public class ObservationInfoBuilder extends Builder {

	public static void build(ContinuityOfCareDocument ccd, QOMOP qomop) {

		List<com.quintiles.omop.Observation> observationList = new ArrayList<com.quintiles.omop.Observation>();

		handleGEGeneralObservations(observationList, ccd);
		handleResultSection(observationList, ccd);
		handleVitalSection(observationList, ccd);
		handleSocialHistorySection(observationList, ccd);

		qomop.setObservationList(observationList);

	}

	public static void handleGEGeneralObservations(
			List<com.quintiles.omop.Observation> observationList,
			ContinuityOfCareDocument ccd) {
		for (Section section : ccd.getSections()) {

			if (section.getTitle().getText()
					.equalsIgnoreCase("GE General Observations")
					|| (OMOPUtils.getTemplateId(section) != null && OMOPUtils
							.getTemplateId(section).equalsIgnoreCase(
									"1.2.840.113619.21.3.2529"))) {
				for (Entry entry : section.getEntries()) {
					Observation ob = entry.getObservation();
					if (ob != null) {
						handleObservation(observationList, ccd, ob);
					}
				}
			}
		}
	}

	public static void populateObSrcKey(Observation ccd_ob,
			com.quintiles.omop.Observation qObservation) {
		String authoredTime = null;
		if (ccd_ob.getAuthors().size() > 0) {
			if (ccd_ob.getAuthors().get(0).getTime() != null) {
				authoredTime = ccd_ob.getAuthors().get(0).getTime().getValue();
			}
		}
		if (ccd_ob.getIds().size() > 0) {
			II id = ccd_ob.getIds().get(0);
			qObservation.setSourceKey(OMOPUtils.getId(id, authoredTime));
		}
	}

	public static void handleSocialHistorySection(
			List<com.quintiles.omop.Observation> observationList,
			ContinuityOfCareDocument ccd) {
		if (ccd.getSocialHistorySection() != null
				&& ccd.getSocialHistorySection().getObservations() != null) {
			for (Observation ccd_ob : ccd.getSocialHistorySection()
					.getObservations()) {
				handleObservation(observationList, ccd, ccd_ob);
			}
		}
	}

	public static void handleResultSection(
			List<com.quintiles.omop.Observation> observationList,
			ContinuityOfCareDocument ccd) {
		if (ccd.getResultsSection() != null
				&& ccd.getResultsSection().getOrganizers() != null) {
			EList<Organizer> resultOrganizers = ccd.getResultsSection()
					.getOrganizers();
			for (Organizer org : resultOrganizers) {
				for (Observation ccd_ob : org.getObservations()) {
					handleObservation(observationList, ccd, ccd_ob);
				}
			}
		}

	}

	public static void handleVitalSection(
			List<com.quintiles.omop.Observation> observationList,
			ContinuityOfCareDocument ccd) {
		if (ccd.getVitalSignsSectionEntriesOptional() != null
				&& ccd.getVitalSignsSectionEntriesOptional().getOrganizers() != null) {
			EList<Organizer> vitalOrganizers = ccd
					.getVitalSignsSectionEntriesOptional().getOrganizers();
			for (Organizer org : vitalOrganizers) {
				for (Observation ccd_ob : org.getObservations()) {
					handleObservation(observationList, ccd, ccd_ob);
				}
			}
		}
	}

	public static void handleObservation(
			List<com.quintiles.omop.Observation> observationList,
			ContinuityOfCareDocument ccd, Observation ccd_ob) {
		if (isValidEntry(ccd_ob.getNullFlavor())) {
			com.quintiles.omop.Observation qomop_ob = new com.quintiles.omop.Observation();
			//qomop_ob.setObservationId(Long.toString(OMOPUtils.generateUniqueID()));
			qomop_ob.setCreateDate(Builder.getFormattedDate((ccd
					.getEffectiveTime().getValue())));
			qomop_ob.setPersonId(OMOPUtils.getPersonId(ccd));
			qomop_ob.setSourceMemberId(OMOPUtils.getMemberId(ccd));
			qomop_ob.setKindOf(OMOPUtils.getKindOf(ccd_ob));
			populateObSrcKey(ccd_ob, qomop_ob);
			String hashValue = OMOPUtils.getHashValue(qomop_ob.getSourceKey());
			qomop_ob.setObservationId(hashValue);
			
			
			
			if (ccd_ob.getCode() != null) {
				if (ccd_ob.getCode().getNullFlavor() != null) {
					qomop_ob.setObservationCdNull(ccd_ob.getCode()
							.getNullFlavor().getName());
				}
				qomop_ob.setObservationCdSys(getVocabSys(ccd_ob.getCode().getCodeSystem()));
				qomop_ob.setObservationCdSysName(ccd_ob.getCode()
						.getCodeSystemName());
				qomop_ob.setObservationCdValue(getCodeValue(ccd_ob.getCode().getCode()));
				qomop_ob.setObservationSourceValue(ccd_ob.getCode()
						.getDisplayName());
			}

			if (ccd_ob.getEffectiveTime() != null) {
				if (ccd_ob.getEffectiveTime().getLow() != null) {
					qomop_ob.setObservationStartDate(getFormattedDate(ccd_ob
							.getEffectiveTime().getLow().getValue()));
					qomop_ob.setObservationDate(getFormattedDate(ccd_ob
							.getEffectiveTime().getLow().getValue()));
				}
				if (ccd_ob.getEffectiveTime().getHigh() != null) {
					qomop_ob.setObservationEndDate(getFormattedDate(ccd_ob
							.getEffectiveTime().getHigh().getValue()));
				}
			}
			handleObservationValues(ccd_ob, qomop_ob);
			handleObservationRange(ccd_ob, qomop_ob);
			/*
			 * Extract associated providerId if exists.
			 */
			if (ccd_ob.getPerformers().size() > 0) {
				qomop_ob.setAssociatedProviderId(extractProviderId(ccd_ob
						.getPerformers().get(0)));
			}
			observationList.add(qomop_ob);
		}
	}

	private static void handleObservationValues(
			org.openhealthtools.mdht.uml.cda.Observation ccd_ob,
			com.quintiles.omop.Observation qomop_ob) {
		if (ccd_ob.getValues().size() > 0) {
			Object value = ccd_ob.getValues().get(0);

			if (value instanceof CD) {
				if (((CD) value).getNullFlavor() != null) {
					qomop_ob.setValueCdNull(((CD) value).getNullFlavor()
							.getName());
				}
				qomop_ob.setValueCdSys(getVocabSys(((CD) value).getCodeSystem()));
				// Need to correct the property name(valueCdName) 'sys' is
				// missing
				qomop_ob.setValueCdName(((CD) value).getCodeSystemName());
				qomop_ob.setValueCdValue(getCodeValue( ((CD) value).getCode() ));
			} else if (value instanceof PQ) {
				PQ reading = (PQ) value;
				if (reading.getValue() != null) {
					qomop_ob.setValueAsNumber(reading.getValue().toString());
				}
				qomop_ob.setUnitsSourceValue(reading.getUnit());
			} else if (value instanceof ST) {
				// values like postivie or negative
				ST reading = (ST) value;
				qomop_ob.setValueAsString(reading.getText());
			}else{
				qomop_ob.setValueCdSys("UNK_VOC");
				qomop_ob.setValueCdValue("UNK_CD");
			}
		}
	}

	private static void handleObservationRange(
			org.openhealthtools.mdht.uml.cda.Observation ccd_ob,
			com.quintiles.omop.Observation qomop_ob) {

		if (ccd_ob.getReferenceRanges().size() > 0) {
			ReferenceRange range = ccd_ob.getReferenceRanges().get(0);
			if (range.getObservationRange().getText() != null
					&& range.getObservationRange().getText().getText() != null) {
				String s = range.getObservationRange().getText().getText();
				s = s.replace("(", "");
				s = s.replace(")", "");
				String[] split = s.split("-");
				if (split.length == 2) {
					String lowRange = split[0];
					String highRange = split[1];
					qomop_ob.setRangeHigh(highRange);
					qomop_ob.setRangeLow(lowRange);
				}
			}
		}

	}

}
