package com.quintiles.qomop.utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * This utility class provides a method to assist in creating delimited output.
 */
public final class DelimitedUtility
{
	/**
	 * Add a value to the buffer of delimited values.
	 * 
	 * @param stringBuilderParm required
	 * @param valueParm ignored if null
	 */
	public static void appendDelimited(final StringBuilder stringBuilderParm, final Object valueParm)
	{
		Calendar calendar = null;

		// Add a delimiter, except for the first entry
		if (stringBuilderParm.length() > 0)
		{
			stringBuilderParm.append("\01");
			//stringBuilderParm.append("|");
		}

		// Skip the value if it is null
		if (valueParm != null)
		{
			if (valueParm instanceof Calendar)
			{
				calendar = (Calendar) valueParm;
				SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				stringBuilderParm.append(formater.format(calendar.getTime()));
				return;
			}
			//Escapes \r \n characters under the string.
			String escString = StringEscapeUtils.escapeJava(valueParm.toString());
			if (escString.length() > 200)
			{
				stringBuilderParm.append(escString.substring(0, 200));
			}
			else
			{
				stringBuilderParm.append(escString);
			}
		}
	}

	/**
	 * Hidden constructor
	 */
	private DelimitedUtility()
	{
	}

}
