package com.quintiles.qomop.utility;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.openhealthtools.mdht.uml.cda.Observation;
import org.openhealthtools.mdht.uml.cda.Organization;
import org.openhealthtools.mdht.uml.cda.PatientRole;
import org.openhealthtools.mdht.uml.cda.Section;
import org.openhealthtools.mdht.uml.cda.consol.ContinuityOfCareDocument;
import org.openhealthtools.mdht.uml.hl7.datatypes.AD;
import org.openhealthtools.mdht.uml.hl7.datatypes.II;
import org.openhealthtools.mdht.uml.hl7.datatypes.TS;

import com.quintiles.omop.Location;
import com.quintiles.qomop.builders.Builder;
import com.quintiles.qomop.builders.Builder.OrgInfo;

public class OMOPUtils {
	
	/*
	 * Performs hashing on the given sourceKey
	 */
	public static String getHashValue(String sourceKey){
		if(sourceKey!= null)
			return Integer.toString(sourceKey.hashCode());
		else
		return null;
		
	}

	public static Location getLocation(final AD address,ContinuityOfCareDocument ccd) {

		Location loc = new Location();
		loc.setLocationSid(Long.toString(OMOPUtils.generateUniqueID())); // uniquely
																			// generated
		if (address.getStreetAddressLines().size() > 0) {
			loc.setAddress1(address.getStreetAddressLines().get(0).getText());
		}
		if (address.getStreetAddressLines().size() >= 2) {
			loc.setAddress2(address.getStreetAddressLines().get(1).getText());
		}
		if (address.getCities().size() > 0) {
			loc.setCity(address.getCities().get(0).getText());
		}
		if (address.getStates().size() > 0) {
			loc.setState(address.getStates().get(0).getText());
		}
		if (address.getPostalCodes().size() > 0) {
			loc.setZip(address.getPostalCodes().get(0).getText());
		}
		if (address.getCounties().size() > 0) {
			loc.setCounty(address.getCounties().get(0).getText());
		}
		
		loc.setCreateDate(Builder.getFormattedDate((ccd.getEffectiveTime().getValue())));
		loc.setPersonId(OMOPUtils.getPersonId(ccd));
		loc.setSourceMemberId(OMOPUtils.getMemberId(ccd));
		return loc;
	}

	public static String getKindOf(Observation ccd_ob) {
		if (ccd_ob.getTemplateIds() != null
				&& ccd_ob.getTemplateIds().size() > 0) {
			String kindOf = Builder.observationTypeMap.get(ccd_ob
					.getTemplateIds().get(0).getRoot());
			if (kindOf != null) {
				return kindOf;
			}
		}
		return "GE General Observation";
	}

	public static String getTemplateId(Section section) {
		if (section.getTemplateIds() != null
				&& section.getTemplateIds().size() > 0) {
			if (section.getTemplateIds().get(0).getRoot() != null)
				return section.getTemplateIds().get(0).getRoot();
		}
		return null;
	}

	public static String getMemberId(ContinuityOfCareDocument ccd) {
		if (ccd.getPatientRoles().size() > 0) {
			if (ccd.getPatientRoles().get(0).getProviderOrganization().getIds()
					.size() > 0) {
				EList<II> ids = ccd.getPatientRoles().get(0)
						.getProviderOrganization().getIds();
				for (II id : ids) {
					if (id.getRoot().equalsIgnoreCase("2.16.840.1.113883.19.1"))
						return id.getExtension();
				}
			}
		}
		return null;

	}

	// returns the ID for the node as root:extension
	public static String getId(final II identifier, final TS time) {
		if (identifier != null && time != null) {
			if (identifier.getExtension() != null
					&& !identifier.getExtension().equals(""))
				return identifier.getRoot() + ":" + identifier.getExtension()
						+ ":" + time.getValue(); // extension is optional
			else
				return identifier.getRoot() + ":" + time.getValue();
		}
		return null;
	}

	public static String getId(final II identifier) {
		if (identifier != null) {
			return identifier.getRoot() + ":" + identifier.getExtension();
		}
		return null;
	}

	public static String getId(II id, String authoredTime) {
		if (id != null) {
			if (id.getRoot() != null && id.getExtension() == null) {
				return id.getRoot();
			}
			if (id.getRoot() != null && id.getExtension() != null
					&& authoredTime == null) {
				return id.getRoot() + ":" + id.getExtension();
			}
			if (id.getRoot() != null && id.getExtension() != null
					&& authoredTime != null) {
				return id.getRoot() + ":" + id.getExtension() + ":"
						+ authoredTime;
			}

		}
		return null;
	}

	/*
	 * This returns a string concatenated with memberID with leftpadded LEPID 
	 */
	public static String getPersonId(final ContinuityOfCareDocument ccd) {
		EList<PatientRole> patientRoles = ccd.getPatientRoles();
		if ((patientRoles != null) && (patientRoles.size() > 0)) {
			for (PatientRole patientRole : patientRoles) {
				return extractPatientId(patientRole);
				/*String memberId = getMemberId(patientRole);
				if(memberId!=null){
				    return memberId+StringUtils.leftPad(extractPatientId(patientRole),20,"0");
				}else{
					 return StringUtils.leftPad(extractPatientId(patientRole),25,"0");
				}*/
			}
		}
		return null;
	}
	
	
	public static String getMemberId(PatientRole patientRole){
		
		Organization org = patientRole.getProviderOrganization();
		if (org != null)
		{
			for (II id : org.getIds())
			{
				if (id.getRoot().equals("2.16.840.1.113883.19.2"))
				{
					return id.getExtension();
				}			
			}
		}
		return null;
	}
	
	
	public static String extractPatientId(PatientRole patientRole) {
		// Patient Id is found @root whose ending letters are 1.1.1
		Pattern pattern = Pattern.compile(Builder.PATIENT_ROOT_PATTRN);

		for (II id : patientRole.getIds()) {
			if (id.getRoot() != null) {
				Matcher matcher = pattern.matcher(id.getRoot());
				if (matcher.matches()) {
					return id.getExtension();
				}
			}
		}
		return null;
	}

	
	public static long generateUniqueID() {
		return UUID.randomUUID().getMostSignificantBits();
	}
}