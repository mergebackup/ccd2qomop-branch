package com.quintiles.qomop.utility;

import java.util.UUID;

/**
 * This utility class generates unique BigInteger values for use as surrogate keys. The BigInteger value returned is
 * guaranteed to fit in a Number(38) field.
 */
public final class GenerateId
{

	/**
	 * Get the next key value. This method is thread-safe.
	 * 
	 * @return
	 */
	public static String next()
	{

		return UUID.randomUUID().toString();
	}

	/**
	 * Hidden constructor
	 */
	private GenerateId()
	{
		// Do nothing
	}
}
